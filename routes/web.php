<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// if (App::environment('production')) {
//     URL::forceScheme('https');
// }
Route::get('/', function () {
    return view('flashcard.home');
});

Route::get('/courses', function () {
    return view('flashcard.courses');
});

Route::get('/login', function () {
    return view('flashcard.login');
});

Route::get('/register', function () {
    return view('flashcard.register');
});

Route::get('/packages', function () {
    return view('flashcard.packages');
});

Route::get('/detail-courses', function () {
    return view('flashcard.detail-courses');
});

Route::get('/intro', function () {
    return view('flashcard.intro');
});

Route::get('/learning', function () {
    return view('flashcard.learning');
});

Route::get('/learning-finish', function () {
    return view('flashcard.learning_finish');
});

Route::get('/show-filter', function () {
    return view('flashcard.show-filter');
});

Route::get('/test', function () {
    return view('flashcard.test');
});

Route::get('/payment-step-1', function () {
    return view('flashcard.payment-step-1');
});


Route::get('/payment-step-2', function () {
    return view('flashcard.payment-step-2');
});

Route::get('/final', function () {
    return view('flashcard.final');
});

// Route::get('/','PagesController@index');
// Route::get('/about','PagesController@about');
// Route::get('/services','PagesController@services');
// Route::get('/study','StudyController@index');

//create route for every methods in FlashcardsController
// Route::resource('flashcards','FlashcardsController');
// Route::get('/dash',function(){
// 	return view('dashboard');
// });
/*Route::get('eFlashcard',function(){
	return 'xin chao cac ban';
});


Route::get('HoTen/{ten}',function($ten)
{
	echo ("Ten cua ban la: ".$ten);
});


Route::get('Ngay/{ngay}',function($ngay){
	echo "Hom nay la ngay" .$ngay;
})->where(['ngay'=>'[]+']);*/


 //Auth::routes();

 Route::get('/home', 'HomeController@index')->name('home');


