<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *Must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title -->
    <title>Introdution</title>

    <!-- Favicon -->
    <link rel="icon" href="img/core-img/favicon.ico">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <!-- Stylesheet -->
    <link rel="stylesheet" href="style.css">

</head>

<body>
    <div id="voca-wrap">
        <div class="main-wrapper">
            <div class="payment-header">
                <div class="payment-header-content">
                    <div class="payment-header-left">

                        <a class="btn-back" href="#">
                            <p> eFlashcard</p>
                        </a>
                    </div>
                    <div class="payment-header-center">
                        <div class="header-center-content">
                            <div class="center-content-step">
                                <span class="content-step-1">Thông tin của bạn</span>
                                <span class="content-step-2">Thanh toán &amp; học ngay</span>
                            </div>
                            <div class="clearfix"></div>
                            <div class="center-content-progress">
                                <span class="content-progress-1 progess-active">1</span>
                                <span class="content-progress-2">2</span>
                            </div>
                        </div>
                    </div>
                    <div class="payment-header-right">
                        <div class="header-right-content">

                            <p class="right-content-hotline"><strong>Liên hệ:</strong> 082 990 58 58</p>
                            <p class="right-content-time"><strong>Làm việc:</strong> 8-21h, cả T7 &amp; CN</p>
                        </div>
                    </div>
                </div>

            </div>
            <div class="payment-group">
                <div class="payment-content">
                    <div class="">
                        <h1 class="payment-title">1. Thông tin của bạn</h1>
                        <h2 class="payment-sub-title">Mời bạn nhập chính xác họ tên và số điện thoại, thông tin này sẽ giúp eFlashcard hỗ trợ tốt hơn nếu có sự cố thanh toán xảy ra.</h2>
                    </div>
                    <div class="payment-content-item">
                        <div class="payment-content-left">
                            <!-- <div class="content-left-image">
                  <img src="https://www.voca.vn/assets/img/vocabulary-home.png" alt="voca payment">
              </div> -->
                            <div class="content-left-form">
                                <form action="https://www.voca.vn/payment/premium?step=2" method="post">
                                    <input type="hidden" value="0" name="payment_method">
                                    <p>Họ tên</p>
                                    <input type="text" value="" name="full_name" pattern=".{3,}" placeholder="Nhập họ tên" required="required" autocomplete="off">
                                    <p>Số điện thoại</p>
                                    <input type="text" value="" pattern="[\d]{10,15}" title="Nhập số điện thoại hợp lệ" name="phone_number" placeholder="Nhập số điện thoại" required="required" autocomplete="off">
                                    <button>Bước tiếp theo</button>
                                </form>
                            </div>
                        </div>
                        <div class="payment-content-right">
                            <div class="premium-package">
                                <div class="premium-item">
                                    <p>thông tin gói được chọn mua hiện tại</p>

                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <footer id='contact' class="site-footer">
        <div class="footer-widgets">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-6 col-lg-3">
                        <div class="foot-about">
                            <a class="foot-logo" href="#">
                                <p>eFlashcard</p>
                            </a>

                            <p style="color: burlywood;">Website eFlashcard cung cấp đến học viên đầy đủ các chủ đề. Giúp các học viên tiếp cận cách học tiếng anh hiệu quả nhất.</p>

                            <p class="footer-copyright">
                                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                Copyright &copy;
                                <script>
                                    document.write(new Date().getFullYear());
                                </script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="#" target="_blank">eFlashcard</a>
                                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            </p>
                        </div>
                        <!-- .foot-about -->
                    </div>
                    <!-- .col -->

                    <div class="col-12 col-md-6 col-lg-3 mt-5 mt-md-0">
                        <div class="foot-contact">
                            <h2>Contact Us</h2>

                            <ul>
                                <li>Email: info.eflashcard@gmail.com</li>
                                <li>Phone: (+84) 369 791 967</li>
                                <li>Address: Khu phố 6, phường Linh Trung, quận Thủ Đức</li>
                            </ul>
                        </div>
                        <!-- .foot-contact -->
                    </div>
                    <!-- .col -->

                    <div class="col-12 col-md-6 col-lg-3 mt-5 mt-lg-0">
                        <div class="quick-links flex flex-wrap">
                            <h2 class="w-100">Quick Links</h2>

                            <ul class="w-50">
                                <li><a href="courses.html">Courses </a></li>
                                <li><a href="#member">Members </a></li>
                                <li><a href="#contact">Contact Us</a></li>
                            </ul>


                        </div>
                        <!-- .quick-links -->
                    </div>
                    <!-- .col -->

                    <div class="col-12 col-md-6 col-lg-3 mt-5 mt-lg-0">
                        <div class="follow-us">
                            <h2>Follow Us</h2>

                            <ul class="follow-us flex flex-wrap align-items-center">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            </ul>
                        </div>
                        <!-- .quick-links -->
                    </div>
                    <!-- .col -->
                </div>
                <!-- .row -->
            </div>
            <!-- .container -->
        </div>
        <!-- .footer-widgets -->


        <!-- .footer-bar -->
    </footer>
</body>

</html>