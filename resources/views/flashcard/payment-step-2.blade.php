<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *Must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title -->
    <title>Introdution</title>

    <!-- Favicon -->
    <link rel="icon" href="img/core-img/favicon.ico">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <!-- Stylesheet -->
    <link rel="stylesheet" href="style.css">

</head>

<body>
    <div id="voca-wrap">
        <div class="main-wrapper">
            <div class="payment-header">
                <div class="payment-header-content">
                    <div class="payment-header-left">

                        <a class="btn-back" href="#">
                            <p> <i class="fas fa-chevron-left"></i> eFlashcard</p>
                        </a>
                    </div>
                    <div class="payment-header-center">
                        <div class="header-center-content">
                            <div class="center-content-step">
                                <span class="content-step-1">Thông tin của bạn</span>
                                <span class="content-step-2">Thanh toán &amp; học ngay</span>
                            </div>
                            <div class="clearfix"></div>
                            <div class="center-content-progress">
                                <span class="content-progress-1 progess-active">1</span>
                                <span class="content-progress-2">2</span>
                            </div>
                        </div>
                    </div>
                    <div class="payment-header-right">
                        <div class="header-right-content">

                            <p class="right-content-hotline"><strong>Liên hệ:</strong> 082 990 58 58</p>
                            <p class="right-content-time"><strong>Làm việc:</strong> 8-21h, cả T7 &amp; CN</p>
                        </div>
                    </div>
                </div>

            </div>
            <div class="payment-group">
    <div class="payment-content">
        <div class="">
            <h1 class="payment-title">2. Thanh toán &amp; học ngay</h1>
           
        </div>
        <div class="payment-content-item">
          
          <div class="payment-content-left">
              <form class="payment-form" action="https://www.voca.vn/payment/premium" method="post">
                <input type="hidden" name="_token" value="qk8wJTHRwNy29Uy2uO7qDkn7rT72iMGkUs380pXn">
                <input type="hidden" value="Nguyễn Kim Thảo" name="full_name" pattern=".{3,}" placeholder="Nhập họ tên" required="required" autocomplete="off">
                <input type="hidden" value="01338124756" pattern="[\d]{10,15}" title="Nhập số điện thoại hợp lệ" name="phone_number" placeholder="Nhập số điện thoại" required="required" autocomplete="off">
                <input type="hidden" name="promotion_code" value="" link="https://www.voca.vn/check-promotion-code/1">
                <input type="hidden" id="promotion_discount" value="0">
              <div class="content-left-method">
                 
                      <div class="content-method">
                          <p class="method-label">
                            <label class="radio-label">Nhận thẻ eFlashcard và thanh toán tại nhà
                              <input type="radio" checked="checked" value="7" name="payment_method" val="G_COD">
                              <span class="checkmark"></span>
                            </label>
                          </p>
                          <div class="method-item method-home method-active">
                              <p>Địa chỉ giao hàng</p>
                              <input type="text" name="address" pattern=".{3,}" placeholder="Nhân viên giao hàng VOCA sẽ giao chính xác thẻ cho bạn theo địa chỉ cung cấp" autocomplete="off">
                              <p class="address-msg">Nhân viên sẽ gọi điện xác nhận thông qua số điện thoại mà bạn cung cấp. Sau đó nhân viên giao hàng eFlashcard sẽ giao chính xác thẻ cho bạn theo địa chỉ này.</p>
                          </div>
                          <p class="method-label">
                            <label class="radio-label">Nhận mã code trực tiếp qua mail
                              <input type="radio" value="3" name="payment_method" val="G_CK">
                              <span class="checkmark"></span>
                            </label>
                          </p>
                          <div class="method-item">
                               <!-- Bank Transfer -->
                                <div class="payment-bank-transfer">
                                    <div class="payment-bank-transfer-content">
                                    <p class="payment-step-label"><span>Bước 1:</span>Bạn chuyển khoản thanh toán theo thông tin sau:</p>

                                    <div class="payment-info-account">
                                        <p class="payment-info-detail">Số tài khoản: <span>0531002470053</span></p>
                                        <p class="payment-info-detail">Chủ tài khoản: <span>Lê Hà Thị Mỹ Linh</span></p>
                                        <p class="payment-info-detail">Ngân hàng: <span>Ngân hàng Vietcombank chi nhánh Đông Sài Gòn. (hoặc chi nhánh Bình Thạnh)</span></p>
                                        <p class="payment-info-detail">Nội dung: <span class="no-semibold">Email(nếu có) - Số điện thoại - Tên khóa học<br>Ví dụ: nguyenvana@gmail.com - 0123456789 - VOCA PREMIUM</span></p>
                                    </div>

                                    <p class="payment-step-label"><span>Bước 2:</span>Hệ thống sẽ tự động kích hoạt cho bạn trong vòng 24h.</p>
                                    </div>
                                    <p class="payment-info-label">Lưu ý:</p>
                                    <p class="payment-info-detail">Nếu Anh/chị chuyển khoản cùng Ngân hàng thì VOCA sẽ nhận được thanh toán ngay, và kích hoạt sớm.</p>
                                    <p class="payment-info-detail">Nếu khác ngân hàng và chi nhánh thì giao dịch thường sau 2 tiếng mới hoàn thành.</p>
                                </div>
                          </div>
							                      </div>
                  
              </div>

              <div class="content-left-promotion">
                  <div class="content-promotion-form">
                      <span>Mã giảm giá</span>
                      <span class="promotion-input">
                        <input type="text" class="promotion-input" placeholder="Nhập ở đây...">
                        <a href="javascript:void(0)" class="btn-promotion">Đồng ý</a>
                      </span>
                  </div>
                 
              </div>
              <div class="content-left-button">
			  
				   						
			  
                  <button id="premium" onclick="return formConfirm();">Đặt mua</button>
                  <p>(Vui lòng kiểm tra đơn hàng trước khi Đặt mua)</p>
              </div>
              </form>
          </div>
          <div class="payment-content-right">
              <div class="content-right-info">
                  <div class="right-info">
                      <p>Thông tin của bạn
                          <a href="https://www.voca.vn/payment/premium?step=1&amp;t=1575886004">Sửa</a>
                      </p>
                  </div>
                  <div class="right-name">
                      <p class="right-user-name">Nguyễn Kim Thảo</p>
                      <p class="right-hotline">Điện thoại: 01338124756</p>
                  </div>
              </div>

                            
              <input type="hidden" class="pay-free-ship" value="1900000" name="">
              <div class="content-right-info">
                  <div class="right-info">
                      <p>Đơn hàng
                      </p>
                  </div>
                  <div class="right-product">
                      <div class="right-product-name">
                          <p class="product-name"><strong>PREMIUM</strong> <span class="product-price">2.199.000 đ</span></p>
                          <p class="product-description">Tài khoản học tiếng Anh cao cấp nhất trên nền tảng VOCA.VN, cho phép bạn được học tất cả khóa học tiếng Anh trên cả 5 hệ thống</p>
						  <p class="price-store" style="font-size: 12px;font-family: opensansregular;text-decoration: line-through;display: block;width: 100%;text-align: right;color: #f00;margin-top: -10px;margin-bottom: 0;"></p>
                      </div>
                      <p class="product-discount">Giảm giá <span>0 đ</span></p>
                      <p class="product-coin hidden" val="0">Khấu trừ ME <span>0 đ</span></p>
                      <div class="product-line"></div>
                      <p class="product-value">Tạm tính <span>2.199.000 đ</span></p>
                      <p class="product-ship" val="0">Phí vận chuyển <span>0 đ</span></p>
                      <div class="product-mount-line"></div>
                      <p class="product-amount">Thành tiền <span>2.199.000 đ</span></p>
                      
                      <p class="product-vat">(Đã bao gồm VAT)</p>
                      <div class="clearfix"></div>
                  </div>

              </div>
            
          </div>
          <div class="clearfix"></div>
        </div>
        
    </div>
  </div>
        </div>
    </div>
    <footer id='contact' class="site-footer">
        <div class="footer-widgets">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-6 col-lg-3">
                        <div class="foot-about">
                            <a class="foot-logo" href="#">
                                <p>eFlashcard</p>
                            </a>

                            <p style="color: burlywood;">Website eFlashcard cung cấp đến học viên đầy đủ các chủ đề. Giúp các học viên tiếp cận cách học tiếng anh hiệu quả nhất.</p>

                            <p class="footer-copyright">
                                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                Copyright &copy;
                                <script>
                                    document.write(new Date().getFullYear());
                                </script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="#" target="_blank">eFlashcard</a>
                                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            </p>
                        </div>
                        <!-- .foot-about -->
                    </div>
                    <!-- .col -->

                    <div class="col-12 col-md-6 col-lg-3 mt-5 mt-md-0">
                        <div class="foot-contact">
                            <h2>Contact Us</h2>

                            <ul>
                                <li>Email: info.eflashcard@gmail.com</li>
                                <li>Phone: (+84) 369 791 967</li>
                                <li>Address: Khu phố 6, phường Linh Trung, quận Thủ Đức</li>
                            </ul>
                        </div>
                        <!-- .foot-contact -->
                    </div>
                    <!-- .col -->

                    <div class="col-12 col-md-6 col-lg-3 mt-5 mt-lg-0">
                        <div class="quick-links flex flex-wrap">
                            <h2 class="w-100">Quick Links</h2>

                            <ul class="w-50">
                                <li><a href="courses.html">Courses </a></li>
                                <li><a href="#member">Members </a></li>
                                <li><a href="#contact">Contact Us</a></li>
                            </ul>


                        </div>
                        <!-- .quick-links -->
                    </div>
                    <!-- .col -->

                    <div class="col-12 col-md-6 col-lg-3 mt-5 mt-lg-0">
                        <div class="follow-us">
                            <h2>Follow Us</h2>

                            <ul class="follow-us flex flex-wrap align-items-center">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            </ul>
                        </div>
                        <!-- .quick-links -->
                    </div>
                    <!-- .col -->
                </div>
                <!-- .row -->
            </div>
            <!-- .container -->
        </div>
        <!-- .footer-widgets -->


        <!-- .footer-bar -->
    </footer>
</body>

</html>