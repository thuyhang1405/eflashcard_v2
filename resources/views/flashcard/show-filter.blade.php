<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *Must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title -->
    <title>Introdution</title>

    <!-- Favicon -->
    <link rel="icon" href="img/core-img/favicon.ico">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <!-- Stylesheet -->
    <link rel="stylesheet" href="style.css">

</head>

<body>
    <div id="voca-wrap">
        <div class="main-wrapper">
            <div class="learning-main-header promotion-main-header">
                <div class="header-cover">
                    <div class="header-left">
                        <a href="#" class="button-back">
                            <i class="fas fa-backspace"></i> Back</a>
                    </div>
                    <div class="header-right"></div>
                    <div class="header-logo">
                        <a href="">
                            <p>eFlashcard</p>
                        </a>
                  
                    </div>
                    <div class="header-right">
                        <div class="header-avatar">
                            <div class="dropdown">
                                <button type="button">
                                    <a href="">
                                        <img src="img/img_2.jpg" alt="">
                                    </a>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="learning-main-body">
                <div class="filter-result-header">
                    <div class="header-cover">
                        <div class="filter-result-header-left">
                            <h1 class="filter-result-title">MY FRIENDS (LISTENING AND SPEAKING)</h1>
                            <h2 class="filter-result-description">VOCA FOR ENGLISH GRADE 8</h2>
                        </div>
                        <div class="filter-result-header-right">

                            <a href="javascript:void(0)" is_unknow="1" topic="5590" parent="5589" class="filter-result-button button-ranking button-learning-now">Học từ vựng</a>
                        </div>

                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs filter-result-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#general" aria-controls="general" role="tab" data-toggle="tab">Kết quả sàng lọc</a></li>
                            <span class="active-bar" style="width: 138px; left: 0px;"></span>
                        </ul>
                    </div>
                </div>
                <div class="filter-result-body">
                    <div class="body-cover">
                        <!-- Tab panes -->

                        <div class="tab-content filter-result-tab-content" style="width: 1137px;">
                            <div role="tabpanel" class="tab-pane  in active" id="general">

                                <div class="filter-result-list-word">
                                    <p class="filter-result-result">Từ bạn không biết <span class="text-red">0</span></p>
                                    <div class="filter-result-word-item  item-red ">
                                        <div class="item-cover">
                                            <div class="item-front">
                                                <img class="item-image" src="http://www.voca.vn/assets/assets-v2/img/library/introduce.jpg" alt="introduction">
                                                <p class="item-text">introduction</p>
                                                <p class="item-type">(n)</p>
                                                <p class="item-text-vi">sự giới thiệu</p>

                                                <span class="sp-rotate"></span>
                                            </div>
                                            <div class="item-back">
                                                <div class="item-back-cover">
                                                    <span class="item-sound main-sound-play" sound_url="https://www.voca.vn/assets/sounds/bsdkq9pskdjvdov9otmabbbghlixtlev3eo3faecuw.mp3"></span>
                                                    <p class="item-example-label">Ví dụ:</p>
                                                    <p class="item-example">You should make the introduction before we start the interview.</p>

                                                    <span class="sp-rotate"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item-tip alert hidden">
                                            <span class="modal-button-close" href="javascript:void(0)" data-dismiss="alert"></span>
                                            <p class="tip-title">Xem thêm thông tin</p>
                                            <p class="tip-text">Bạn xem ví dụ bằng cách nhấn vào thẻ từ vựng.</p>
                                        </div>
                                    </div>


                                </div>

                                <div class="clearfix"></div>
                                <div class="filter-result-list-word">

                                    <p class="filter-result-result">Từ bạn đã biết <span class="text-green">19</span></p>

                                    <div class="filter-result-word-item  item-green ">
                                        <div class="item-cover">
                                            <div class="item-front">
                                                <img class="item-image" src="http://www.voca.vn/assets/assets-v2/img/library/introduce.jpg" alt="introduction">
                                                <p class="item-text">introduction</p>
                                                <p class="item-type">(n)</p>
                                                <p class="item-text-vi">sự giới thiệu</p>

                                                <span class="sp-rotate"></span>
                                            </div>
                                            <div class="item-back">
                                                <div class="item-back-cover">
                                                    <span class="item-sound main-sound-play" sound_url="https://www.voca.vn/assets/sounds/bsdkq9pskdjvdov9otmabbbghlixtlev3eo3faecuw.mp3"></span>
                                                    <p class="item-example-label">Ví dụ:</p>
                                                    <p class="item-example">You should make the introduction before we start the interview.</p>

                                                    <span class="sp-rotate"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item-tip alert hidden">
                                            <span class="modal-button-close" href="javascript:void(0)" data-dismiss="alert"></span>
                                            <p class="tip-title">Xem thêm thông tin</p>
                                            <p class="tip-text">Bạn xem ví dụ bằng cách nhấn vào thẻ từ vựng.</p>
                                        </div>
                                    </div>
                                    <div class="filter-result-word-item  item-green ">
                                        <div class="item-cover">
                                            <div class="item-front">
                                                <img class="item-image" src="http://www.voca.vn/assets/assets-v2/img/library/guy.jpg" alt="guy">
                                                <p class="item-text">guy</p>
                                                <p class="item-type">(n)</p>
                                                <p class="item-text-vi">anh chàng, gã</p>

                                                <span class="sp-rotate"></span>
                                            </div>
                                            <div class="item-back">
                                                <div class="item-back-cover">
                                                    <span class="item-sound main-sound-play" sound_url="https://www.voca.vn/assets/sounds/zv2iuuce9l8qjdlojksqs1wpsg73bxdwdfyc4fhbyzc.mp3"></span>
                                                    <p class="item-example-label">Ví dụ:</p>
                                                    <p class="item-example">I’m calling a guy like you.</p>

                                                    <span class="sp-rotate"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="filter-result-word-item  item-green ">
                                        <div class="item-cover">
                                            <div class="item-front">
                                                <img class="item-image" src="http://www.voca.vn/assets/assets-v2/img/library/Sweden.jpg" alt="Sweden">
                                                <p class="item-text">Sweden</p>
                                                <p class="item-type">(n)</p>
                                                <p class="item-text-vi">Thụy Điển</p>

                                                <span class="sp-rotate"></span>
                                            </div>
                                            <div class="item-back">
                                                <div class="item-back-cover">
                                                    <span class="item-sound main-sound-play" sound_url="https://www.voca.vn/assets/sounds/jkqppwfpuxzi1nvo8syw63zadewfbr1ngzpi19q9xm.mp3"></span>
                                                    <p class="item-example-label">Ví dụ:</p>
                                                    <p class="item-example">Sweden is next to Denmark.</p>

                                                    <span class="sp-rotate"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="filter-result-word-item  item-green ">
                                        <div class="item-cover">
                                            <div class="item-front">
                                                <img class="item-image" src="http://www.voca.vn/assets/assets-v2/img/library/compare_1.jpg" alt="compare">
                                                <p class="item-text">compare</p>
                                                <p class="item-type">(v)</p>
                                                <p class="item-text-vi">so sánh</p>

                                                <span class="sp-rotate"></span>
                                            </div>
                                            <div class="item-back">
                                                <div class="item-back-cover">
                                                    <span class="item-sound main-sound-play" sound_url="https://www.voca.vn/assets/sounds/w0noy9qrbfl50c1bd8nrdiig57ldrtx6q1buuwblji.mp3"></span>
                                                    <p class="item-example-label">Ví dụ:</p>
                                                    <p class="item-example">It is interesting to compare their situation and ours.</p>

                                                    <span class="sp-rotate"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="filter-result-word-item  item-green ">
                                        <div class="item-cover">
                                            <div class="item-front">
                                                <img class="item-image" src="http://www.voca.vn/assets/assets-v2/img/library/country (1).jpg" alt="country">
                                                <p class="item-text">country</p>
                                                <p class="item-type">(n)</p>
                                                <p class="item-text-vi">Đất nước</p>

                                                <span class="sp-rotate"></span>
                                            </div>
                                            <div class="item-back">
                                                <div class="item-back-cover">
                                                    <span class="item-sound main-sound-play" sound_url="https://www.voca.vn/assets/sounds/wnfzyzderoo1asgx40rulrcdzaubnmkv7aelazvvvuk.mp3"></span>
                                                    <p class="item-example-label">Ví dụ:</p>
                                                    <p class="item-example">Which is the largest country in Europe?</p>

                                                    <span class="sp-rotate"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="filter-result-word-item  item-green ">
                                        <div class="item-cover">
                                            <div class="item-front">
                                                <img class="item-image" src="http://www.voca.vn/assets/assets-v2/img/library/far away.jpg" alt="far away">
                                                <p class="item-text">far away</p>
                                                <p class="item-type">(idiom)</p>
                                                <p class="item-text-vi">lâu; xa vời</p>

                                                <span class="sp-rotate"></span>
                                            </div>
                                            <div class="item-back">
                                                <div class="item-back-cover">
                                                    <span class="item-sound main-sound-play" sound_url="https://www.voca.vn/assets/sounds/wl8yryug0ppgnh4o5mx5hid6ppkurlzlyskmuss4kyc.mp3"></span>
                                                    <p class="item-example-label">Ví dụ:</p>
                                                    <p class="item-example">It looks very far away. </p>

                                                    <span class="sp-rotate"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="filter-result-word-item  item-green ">
                                        <div class="item-cover">
                                            <div class="item-front">
                                                <img class="item-image" src="http://www.voca.vn/assets/assets-v2/img/library/Europe.jpg" alt="Europe">
                                                <p class="item-text">Europe</p>
                                                <p class="item-type">(n)</p>
                                                <p class="item-text-vi">Châu Âu</p>

                                                <span class="sp-rotate"></span>
                                            </div>
                                            <div class="item-back">
                                                <div class="item-back-cover">
                                                    <span class="item-sound main-sound-play" sound_url="https://www.voca.vn/assets/sounds/4ptpkcbmgwu1hkivxsjged88uakwkdaxkjdf6ffmk0.mp3"></span>
                                                    <p class="item-example-label">Ví dụ:</p>
                                                    <p class="item-example">She is studying history of Europe.</p>

                                                    <span class="sp-rotate"></span>
                                                </div>
                                            </div>
                                        </div>


                                    </div>

                                    <div class="clearfix"></div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal fade " id="modal-message" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none; padding-right: 25px;">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">

                                <div class="modal-header" style="border-bottom: none;">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="font-size: 30px; color: red;">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>

                                <div class="message-container message-pass">
                                    <div class="message-left">
                                        <div class="message-image">

                                            <img src="img/fade.jpg" alt="img">

                                        </div>


                                    </div>
                                    <div class="message-right">
                                        <p class="message-value">Kết quả sàng lọc:<span class="message-percent">100%</span></p>

                                        <div class="message-detail">
                                            <div class="detail-cover">
                                                <p class="detail-item">Bạn có 19 từ vựng đã biết và 0 từ vựng chưa biết. Giờ bạn có thể chuyển qua bước học từ vựng hoặc xem chi tiết thông tin kết quả sàng lọc.</p>

                                            </div>
                                        </div>

                                        <div class="message-button">
                                            <a href="javascript:void(0)" class="button-item btn-view-detail" data-dismiss="modal">Xem chi tiết</a>

                                            <a href="javascript:void(0)" class="button-item button-learning-now" is_unknow="0" topic="5486" parent="5485" data-dismiss="modal">Học từ vựng</a>

                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- Bootstrap js -->

</body>

</html>