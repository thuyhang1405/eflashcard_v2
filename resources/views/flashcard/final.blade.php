<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *Must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title -->
    <title>Introdution</title>

    <!-- Favicon -->
    <link rel="icon" href="img/core-img/favicon.ico">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <!-- Stylesheet -->
    <link rel="stylesheet" href="style.css">

</head>

<body>
    <div id="voca-wrap">
        <div class="main-wrapper">
            <div class="learning-main-header promotion-main-header">
                <div class="header-cover">
                    <div class="header-left">
                        <a href="#" class="button-back">
                            <i class="fas fa-backspace"></i> Back</a>
                    </div>
                    <div class="header-logo">
                        
                        <a href="#">
                            <p>eFlashcard</p>
                        </a>
                    </div>
                    <div class="header-right">
                        <div class="header-avatar">
                            <div class="dropdown">
                                <button type="button">
                                    <a href="">
                                        <img src="img/img_2.jpg" alt="">
                                    </a>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="learning-main-body">
                <div class="testing-result-header">
                    <div class="header-cover">
                        <div class="testing-result-header-left">
                            <h1 class="testing-result-title">Greeting &amp; Introduction (part 1)</h1>
                            <h2 class="testing-result-description">VOCA FOR KIDS</h2>
                        </div>
                        <div class="testing-result-header-right">
                            <a href="javascript:void(0)" class="testing-result-button button-learning-relearn" is_unknow="0" is_review="0" is_finish="0" topic="5486" parent="5485">Cải thiện</a>

                            <a href="javascript:void(0)" class="testing-result-button button-ranking">Bảng xếp hạng</a>
                        </div>

                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs testing-result-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#general" aria-controls="general" role="tab" data-toggle="tab">Tổng quan</a></li>
                            <li role="presentation" class="hidden"><a href="#detail" aria-controls="detail" role="tab" data-toggle="tab">Chi tiết</a></li>
                            <li role="presentation" class="hidden"><a href="#comment" aria-controls="comment" role="tab" data-toggle="tab">Thảo luận</a></li>
                            <span class="active-bar" style="width: 115px; left: 0px;"></span>
                        </ul>
                    </div>
                </div>

                <div class="testing-result-body">
                    <div class="body-cover">
                        <div class="testing-result-list-word">
                            <p class="testing-result-result">Từ bạn chưa thuộc <span class="text-red">11</span></p>
                            <div class="testing-result-word-item item-red">
                                <div class="item-cover">
                                    <div class="item-front">
                                        <img class="item-image" src="http://www.voca.vn/assets/assets-v2/img/library/country (1).jpg" alt="country">
                                        <p class="item-text">country</p>
                                        <p class="item-type">(n)</p>
                                        <p class="item-text-vi">Đất nước</p>
                                        <span class="sp-rotate"></span>
                                    </div>
                                    <div class="item-back">
                                        <div class="item-back-cover">
                                            <span class="item-sound main-sound-play" sound_url="https://www.voca.vn/assets/sounds/wnfzyzderoo1asgx40rulrcdzaubnmkv7aelazvvvuk.mp3"></span>
                                            <p class="item-example-label">Ví dụ:</p>
                                            <p class="item-example">Which is the largest <span class="example-word">country</span> in Europe?</p>
                                            <span class="sp-rotate"></span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="testing-result-word-item item-red">
                                <div class="item-cover">
                                    <div class="item-front">
                                        <img class="item-image" src="http://www.voca.vn/assets/assets-v2/img/library/far away.jpg" alt="far away">
                                        <p class="item-text">far away</p>
                                        <p class="item-type">(idiom)</p>
                                        <p class="item-text-vi">lâu; xa vời</p>
                                        <span class="sp-rotate"></span>
                                    </div>
                                    <div class="item-back">
                                        <div class="item-back-cover">
                                            <span class="item-sound main-sound-play" sound_url="https://www.voca.vn/assets/sounds/wl8yryug0ppgnh4o5mx5hid6ppkurlzlyskmuss4kyc.mp3"></span>
                                            <p class="item-example-label">Ví dụ:</p>
                                            <p class="item-example">It looks very <span class="example-word">far away</span>.</p>
                                            <span class="sp-rotate"></span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="testing-result-word-item item-red">
                                <div class="item-cover">
                                    <div class="item-front">
                                        <img class="item-image" src="http://www.voca.vn/assets/assets-v2/img/library/guy.jpg" alt="guy">
                                        <p class="item-text">guy</p>
                                        <p class="item-type">(n)</p>
                                        <p class="item-text-vi">anh chàng, gã</p>
                                        <span class="sp-rotate"></span>
                                    </div>
                                    <div class="item-back">
                                        <div class="item-back-cover">
                                            <span class="item-sound main-sound-play" sound_url="https://www.voca.vn/assets/sounds/zv2iuuce9l8qjdlojksqs1wpsg73bxdwdfyc4fhbyzc.mp3"></span>
                                            <p class="item-example-label">Ví dụ:</p>
                                            <p class="item-example">I’m calling a <span class="example-word">guy</span> like you.</p>
                                            <span class="sp-rotate"></span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="testing-result-word-item item-red">
                                <div class="item-cover">
                                    <div class="item-front">
                                        <img class="item-image" src="http://www.voca.vn/assets/assets-v2/img/library/Asia.jpg" alt="Asia">
                                        <p class="item-text">Asia</p>
                                        <p class="item-type">(n)</p>
                                        <p class="item-text-vi">châu Á</p>
                                        <span class="sp-rotate"></span>
                                    </div>
                                    <div class="item-back">
                                        <div class="item-back-cover">
                                            <span class="item-sound main-sound-play" sound_url="https://www.voca.vn/assets/sounds/k57tv83ygriaklkwxwo9zwgwpt3u5n8kimbbbegl8.mp3"></span>
                                            <p class="item-example-label">Ví dụ:</p>
                                            <p class="item-example">Vietnam is in <span class="example-word">Asia</span>.</p>
                                            <span class="sp-rotate"></span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="testing-result-word-item item-red">
                                <div class="item-cover">
                                    <div class="item-front">
                                        <img class="item-image" src="http://www.voca.vn/assets/assets-v2/img/library/Sweden.jpg" alt="Sweden">
                                        <p class="item-text">Sweden</p>
                                        <p class="item-type">(n)</p>
                                        <p class="item-text-vi">Thụy Điển</p>
                                        <span class="sp-rotate"></span>
                                    </div>
                                    <div class="item-back">
                                        <div class="item-back-cover">
                                            <span class="item-sound main-sound-play" sound_url="https://www.voca.vn/assets/sounds/jkqppwfpuxzi1nvo8syw63zadewfbr1ngzpi19q9xm.mp3"></span>
                                            <p class="item-example-label">Ví dụ:</p>
                                            <p class="item-example"><span class="example-word">Sweden</span> is next to Denmark.</p>
                                            <span class="sp-rotate"></span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="testing-result-word-item item-red">
                                <div class="item-cover">
                                    <div class="item-front">
                                        <img class="item-image" src="http://www.voca.vn/assets/assets-v2/img/library/germany%20%281%29.jpg" alt="Germany">
                                        <p class="item-text">Germany</p>
                                        <p class="item-type">(n)</p>
                                        <p class="item-text-vi">nước Đức</p>
                                        <span class="sp-rotate"></span>
                                    </div>
                                    <div class="item-back">
                                        <div class="item-back-cover">
                                            <span class="item-sound main-sound-play" sound_url="https://www.voca.vn/assets/sounds/qu7gngblepiycu7n7yiksb5rmslgkdzzcdazvjghvnw.mp3"></span>
                                            <p class="item-example-label">Ví dụ:</p>
                                            <p class="item-example"><span class="example-word">Germany</span> and Austria are next to each other.</p>
                                            <span class="sp-rotate"></span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="testing-result-word-item item-red">
                                <div class="item-cover">
                                    <div class="item-front">
                                        <img class="item-image" src="http://www.voca.vn/assets/assets-v2/img/library/Korea_1.jpg" alt="Korea">
                                        <p class="item-text">Korea</p>
                                        <p class="item-type">(n)</p>
                                        <p class="item-text-vi">Hàn Quốc</p>
                                        <span class="sp-rotate"></span>
                                    </div>
                                    <div class="item-back">
                                        <div class="item-back-cover">
                                            <span class="item-sound main-sound-play" sound_url="https://www.voca.vn/assets/sounds/bfcqcdctbovaj4uqrk7nqdlvdywxcdy2d7timnpz8.mp3"></span>
                                            <p class="item-example-label">Ví dụ:</p>
                                            <p class="item-example">My father used to travel to <span class="example-word">Korea</span>.</p>
                                            <span class="sp-rotate"></span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="testing-result-word-item item-red">
                                <div class="item-cover">
                                    <div class="item-front">
                                        <img class="item-image" src="http://www.voca.vn/assets/assets-v2/img/library/Europe.jpg" alt="Europe">
                                        <p class="item-text">Europe</p>
                                        <p class="item-type">(n)</p>
                                        <p class="item-text-vi">Châu Âu</p>
                                        <span class="sp-rotate"></span>
                                    </div>
                                    <div class="item-back">
                                        <div class="item-back-cover">
                                            <span class="item-sound main-sound-play" sound_url="https://www.voca.vn/assets/sounds/4ptpkcbmgwu1hkivxsjged88uakwkdaxkjdf6ffmk0.mp3"></span>
                                            <p class="item-example-label">Ví dụ:</p>
                                            <p class="item-example">She is studying history of <span class="example-word">Europe</span>.</p>
                                            <span class="sp-rotate"></span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="testing-result-word-item item-red">
                                <div class="item-cover">
                                    <div class="item-front">
                                        <img class="item-image" src="http://www.voca.vn/assets/assets-v2/img/library/fluently..png" alt="fluent">
                                        <p class="item-text">fluent</p>
                                        <p class="item-type">(adj)</p>
                                        <p class="item-text-vi"> trôi chảy, thông thạo, lưu loát</p>
                                        <span class="sp-rotate"></span>
                                    </div>
                                    <div class="item-back">
                                        <div class="item-back-cover">
                                            <span class="item-sound main-sound-play" sound_url="https://www.voca.vn/assets/sounds/veztltsahup2fctpil5xtxqsxvgtmp1z1jdqaexom2o.mp3"></span>
                                            <p class="item-example-label">Ví dụ:</p>
                                            <p class="item-example">He speaks <span class="example-word">fluent</span> Russian.</p>
                                            <span class="sp-rotate"></span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="testing-result-word-item item-red">
                                <div class="item-cover">
                                    <div class="item-front">
                                        <img class="item-image" src="http://www.voca.vn/assets/assets-v2/img/library/thanks.jpg" alt="thanks">
                                        <p class="item-text">thanks</p>
                                        <p class="item-type">(n)</p>
                                        <p class="item-text-vi">lời cám ơn</p>
                                        <span class="sp-rotate"></span>
                                    </div>
                                    <div class="item-back">
                                        <div class="item-back-cover">
                                            <span class="item-sound main-sound-play" sound_url="https://www.voca.vn/assets/sounds/dzjddalqfmiydl3bz2gdnasnesphlw2v6ddr8kiugy.mp3"></span>
                                            <p class="item-example-label">Ví dụ:</p>
                                            <p class="item-example">I said <span class="example-word">thanks</span> and put the phone down.</p>
                                            <span class="sp-rotate"></span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="testing-result-word-item item-red">
                                <div class="item-cover">
                                    <div class="item-front">
                                        <img class="item-image" src="http://www.voca.vn/assets/assets-v2/img/library/grow up.jpg" alt="grow up">
                                        <p class="item-text">grow up</p>
                                        <p class="item-type">(phrasal verb)</p>
                                        <p class="item-text-vi">lớn lên</p>
                                        <span class="sp-rotate"></span>
                                    </div>
                                    <div class="item-back">
                                        <div class="item-back-cover">
                                            <span class="item-sound main-sound-play" sound_url="https://www.voca.vn/assets/sounds/9ealmxulxlg0sdgfubnxpktpa0uwnozljaqhmmghk.mp3"></span>
                                            <p class="item-example-label">Ví dụ:</p>
                                            <p class="item-example">I <span class="example-word">grew up</span> in country.</p>
                                            <span class="sp-rotate"></span>
                                        </div>
                                    </div>
                                </div>

                            </div>


                        </div>
                        <div class="clearfix"></div>
                        <div class="testing-result-list-word">
                            <p class="testing-result-result">Từ bạn đã thuộc <span class="text-green">8</span></p>
                            <div class="testing-result-word-item item-green">
                                <div class="item-cover">
                                    <div class="item-front">
                                        <img class="item-image" src="http://www.voca.vn/assets/assets-v2/img/library/compare_1.jpg" alt="compare">
                                        <p class="item-text">compare</p>
                                        <p class="item-type">(v)</p>
                                        <p class="item-text-vi">so sánh</p>
                                        <span class="sp-rotate"></span>
                                    </div>
                                    <div class="item-back">
                                        <div class="item-back-cover">
                                            <span class="item-sound main-sound-play" sound_url="https://www.voca.vn/assets/sounds/w0noy9qrbfl50c1bd8nrdiig57ldrtx6q1buuwblji.mp3"></span>
                                            <p class="item-example-label">Ví dụ:</p>
                                            <p class="item-example">It is interesting to <span class="example-word">compare</span> their situation and ours.</p>
                                            <span class="sp-rotate"></span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="testing-result-word-item item-green">
                                <div class="item-cover">
                                    <div class="item-front">
                                        <img class="item-image" src="http://www.voca.vn/assets/assets-v2/img/library/France.jpg" alt="France">
                                        <p class="item-text">France</p>
                                        <p class="item-type">(n)</p>
                                        <p class="item-text-vi">nước Pháp</p>
                                        <span class="sp-rotate"></span>
                                    </div>
                                    <div class="item-back">
                                        <div class="item-back-cover">
                                            <span class="item-sound main-sound-play" sound_url="https://www.voca.vn/assets/sounds/w63nw7w4mfn3yrspp40yc3tcik4wnjnwme6qbguqdg.mp3"></span>
                                            <p class="item-example-label">Ví dụ:</p>
                                            <p class="item-example">The famous towel Effiel is in <span class="example-word">France</span>.</p>
                                            <span class="sp-rotate"></span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="testing-result-word-item item-green">
                                <div class="item-cover">
                                    <div class="item-front">
                                        <img class="item-image" src="http://www.voca.vn/assets/assets-v2/img/library/diplomat.jpg" alt="diplomat">
                                        <p class="item-text">diplomat</p>
                                        <p class="item-type">(n)</p>
                                        <p class="item-text-vi">nhà ngoại giao</p>
                                        <span class="sp-rotate"></span>
                                    </div>
                                    <div class="item-back">
                                        <div class="item-back-cover">
                                            <span class="item-sound main-sound-play" sound_url="https://www.voca.vn/assets/sounds/jdul1ey6yqz88xllbedbxux3bfvavqppchqawmzidg.mp3"></span>
                                            <p class="item-example-label">Ví dụ:</p>
                                            <p class="item-example">You'll need to be a real <span class="example-word">diplomat</span> to persuade them to come to some agreements.</p>
                                            <span class="sp-rotate"></span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="testing-result-word-item item-green">
                                <div class="item-cover">
                                    <div class="item-front">
                                        <img class="item-image" src="http://www.voca.vn/assets/assets-v2/img/library/english.jpg" alt="English">
                                        <p class="item-text">English</p>
                                        <p class="item-type">(n)</p>
                                        <p class="item-text-vi">tiếng Anh</p>
                                        <span class="sp-rotate"></span>
                                    </div>
                                    <div class="item-back">
                                        <div class="item-back-cover">
                                            <span class="item-sound main-sound-play" sound_url="https://www.voca.vn/assets/sounds/kbdrbyu44nplzb6bn120envwyiiprbhmquaf9wjy.mp3"></span>
                                            <p class="item-example-label">Ví dụ:</p>
                                            <p class="item-example">She can speak <span class="example-word">English</span>.</p>
                                            <span class="sp-rotate"></span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="testing-result-word-item item-green">
                                <div class="item-cover">
                                    <div class="item-front">
                                        <img class="item-image" src="http://www.voca.vn/assets/assets-v2/img/library/introduce.jpg" alt="introduction">
                                        <p class="item-text">introduction</p>
                                        <p class="item-type">(n)</p>
                                        <p class="item-text-vi">sự giới thiệu</p>
                                        <span class="sp-rotate"></span>
                                    </div>
                                    <div class="item-back">
                                        <div class="item-back-cover">
                                            <span class="item-sound main-sound-play" sound_url="https://www.voca.vn/assets/sounds/bsdkq9pskdjvdov9otmabbbghlixtlev3eo3faecuw.mp3"></span>
                                            <p class="item-example-label">Ví dụ:</p>
                                            <p class="item-example">You should make the <span class="example-word">introduction</span> before we start the interview.</p>
                                            <span class="sp-rotate"></span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="testing-result-word-item item-green">
                                <div class="item-cover">
                                    <div class="item-front">
                                        <img class="item-image" src="http://www.voca.vn/assets/assets-v2/img/library/What's up.jpg" alt="What's up">
                                        <p class="item-text">What's up</p>
                                        <p class="item-type">(phrase)</p>
                                        <p class="item-text-vi">Xin chào</p>
                                        <span class="sp-rotate"></span>
                                    </div>
                                    <div class="item-back">
                                        <div class="item-back-cover">
                                            <span class="item-sound main-sound-play" sound_url="https://www.voca.vn/assets/sounds/bzyntsrs9nwhwgsxd4hkiqgjphgi2zq7sombyoi6kfe.mp3"></span>
                                            <p class="item-example-label">Ví dụ:</p>
                                            <p class="item-example">He yells to her, '<span class="example-word">What's up</span>?'</p>
                                            <span class="sp-rotate"></span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="testing-result-word-item item-green">
                                <div class="item-cover">
                                    <div class="item-front">
                                        <img class="item-image" src="http://www.voca.vn/assets/assets-v2/img/library/greeting card.jpg" alt="greeting">
                                        <p class="item-text">greeting</p>
                                        <p class="item-type">(n)</p>
                                        <p class="item-text-vi">lời chào hỏi, hành động chào hỏi</p>
                                        <span class="sp-rotate"></span>
                                    </div>
                                    <div class="item-back">
                                        <div class="item-back-cover">
                                            <span class="item-sound main-sound-play" sound_url="https://www.voca.vn/assets/sounds/z8lhrhehiueqrlmabepkywhgfgrm8koju0cskn4qw.mp3"></span>
                                            <p class="item-example-label">Ví dụ:</p>
                                            <p class="item-example">They exchanged <span class="example-word">greetings</span> and sat down to lunch.</p>
                                            <span class="sp-rotate"></span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="testing-result-word-item item-green">
                                <div class="item-cover">
                                    <div class="item-front">
                                        <img class="item-image" src="http://www.voca.vn/assets/assets-v2/img/library/different (1).jpg" alt="different">
                                        <p class="item-text">different</p>
                                        <p class="item-type">(adj)</p>
                                        <p class="item-text-vi">khác, khác biệt, khác nhau</p>
                                        <span class="sp-rotate"></span>
                                    </div>
                                    <div class="item-back">
                                        <div class="item-back-cover">
                                            <span class="item-sound main-sound-play" sound_url="https://www.voca.vn/assets/sounds/b2xdpoxue4cuosykoroo8q9uy6cb1v3x5fnyq0a0ii.mp3"></span>
                                            <p class="item-example-label">Ví dụ:</p>
                                            <p class="item-example">Each of teachers has a <span class="example-word">different</span> teaching method.</p>
                                            <span class="sp-rotate"></span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="modal fade in" id="modal-message" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none; padding-right: 25px;">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">

                            <div class="modal-header" style="border-bottom: none;">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="font-size: 30px; color: red;">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="message-container">
                                <div class="message-left">
                                    <div class="message-image">
                                        <img src="img/sad.png" alt="img">
                                    </div>
                                    <p class="message-title">Rất tiếc!</p>
                                </div>
                                <div class="message-right">
                                    <p class="message-value">Thành tích:<span class="message-percent">42.1%</span></p>

                                    <div class="message-detail">
                                        <div class="detail-cover">
                                            <p class="detail-item">Bạn <span class="text-fail">chưa vượt qua</span> chủ đề từ vựng này.</p>
                                            <p class="detail-item">Để vượt qua, bạn cần đạt kết quả tối thiểu <span class="text-blue-bold">80%</span>. Hãy học lại hoặc kiểm tra lại để có kết quả cao hơn!</p>
                                        </div>
                                    </div>

                                    <div class="message-button">

                                        <a href="javascript:void(0)" class="button-item button-learning-now" is_unknow="0" is_review="0" is_finish="0" topic="5486" parent="5485" data-dismiss="modal">Học lại</a>


                                        <a class="button-item button-retest" href="https://www.voca.vn/test/5486-5485-1">Kiểm tra lại</a>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    </div>


    <!-- Bootstrap js -->

</body>

</html>