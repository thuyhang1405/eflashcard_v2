<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *Must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title -->
    <title>Introdution</title>

    <!-- Favicon -->
    <link rel="icon" href="img/core-img/favicon.ico">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <!-- Stylesheet -->
    <link rel="stylesheet" href="style.css">

</head>

<body>
    <div id="voca-wrap">
        <div class="main-wrapper">
            <div class="learning-main-header promotion-main-header">
                <div class="header-cover">
                    <div class="header-left">
                        <a href="#" class="button-back">
                            <i class="fas fa-backspace"></i> Back</a>
                    </div>
                    
                    <div class="header-logo">
                        <a href="">
                            <p>eFlashcard</p>
                        </a>
                        <p></p>
                    </div>
                    <div class="header-right">
                        <div class="header-avatar">
                            <div class="dropdown">
                                <button type="button">
                                    <a href="">
                                        <img src="img/img_2.jpg" alt="">
                                    </a>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="testing-container">
                <div class="testing-container-cover">
                    <div class="testing-body">
                        <div class="testing-body-cover" total="18">
                            <div class="testing-countdown" id="testing-countdown">
                                <span class="testing-countdown-number">15</span>
                                <svg>
                                    <circle class="circle-shadow" r="16" cx="17" cy="17"></circle>
                                    <circle class="circle" r="16" cx="17" cy="17" stroke="url(#gradientPercent)" style="stroke-dashoffset: -101px;"></circle>
                                    <linearGradient id="gradientPercent" gradientUnits="userSpaceOnUse" x1="0" y1="0" x2="0" y2="34">
                                        <stop offset="0" style="stop-color:#F88600"></stop>
                                        <stop offset="0.5" style="stop-color:#FFB500"></stop>
                                        <stop offset="1" style="stop-color:#FAD300"></stop>
                                    </linearGradient>
                                </svg>
                            </div>
                            <!--pronounce - sound => word -->
                            <div class="testing-exercise   exercise-current " index="0" typequestion="guessing" time="15" type="1">
                                <div class="testing-exercise-flashcard">
                                    <div class="flash-card-viewport">
                                        <div class="flash-card-box-1" id="learning-theory-box-0" index="0">
                                            <figure class="slide-1 jbs-current-1">
                                                <div class="testing-exercise-content">
                                                    <p class="testing-exercise-title">Chọn từ đúng với phiên âm sau:</p>
                                                    <div class="testing-exercise-quesiton" style="text-align: center;">
                                                        <a class="question-sound main-sound-play" href="javascript:void(0)" sound_url="https://www.voca.vn/assets/sounds/itb8znlannkhbsgjmxtirxfrzhx21ceydkrmnhwzpy.mp3"></a>
                                                        <p class="question-phonetic">/ˌkɑːmbɪˈneɪʃn/</p>
                                                    </div>
                                                </div>
                                            </figure>

                                        </div>
                                    </div>
                                </div>
                                <div class="testing-exercise-answer ">
                                    <div class="answer-cover">
                                        <a class="answer-guessing" href="javascript:void(0)" val="1">
                                            <span class="guessing-text">congruent</span>
                                        </a>
                                        <a class="answer-guessing" href="javascript:void(0)" val="2">
                                            <span class="guessing-text">percent</span>
                                        </a>
                                        <a class="answer-guessing" href="javascript:void(0)" val="3">
                                            <span class="guessing-text">horizontal</span>
                                        </a>
                                        <a class="answer-guessing" href="javascript:void(0)" val="4" hideanswer="algebra">
                                            <span class="guessing-text">Đáp án khác</span>
                                        </a>
                                    </div>
                                </div>

                            </div>

                            <div class="testing-exercise    " index="1" typequestion="guessing" time="15" type="2" style="display: none;">

                                <div class="testing-exercise-flashcard">

                                    <div class="flash-card-viewport">
                                        <div class="flash-card-box-1" id="learning-theory-box-1" index="1">

                                            <figure class="slide-1 jbs-current-1">
                                                <div class="testing-exercise-content">
                                                    <p class="testing-exercise-title">Chọn nghĩa đúng với từ vựng sau:</p>
                                                    <div class="testing-exercise-quesiton">
                                                        <p class="question-word"><span>(adj)</span> accurate</p>
                                                    </div>

                                                </div>
                                            </figure>

                                        </div>
                                    </div>

                                </div>

                                <div class="testing-exercise-answer ">
                                    <div class="answer-cover">
                                        <a class="answer-guessing" href="javascript:void(0)" val="1">
                                            <span class="guessing-text">thuật toán, an-go-rít</span>
                                        </a>
                                        <a class="answer-guessing" href="javascript:void(0)" val="2">
                                            <span class="guessing-text">đảo, thuận nghịch</span>
                                        </a>
                                        <a class="answer-guessing" href="javascript:void(0)" val="3">
                                            <span class="guessing-text">diện tích</span>
                                        </a>
                                        <a class="answer-guessing" href="javascript:void(0)" val="4" hideanswer="chính xác, đúng đắn">
                                            <span class="guessing-text">Đáp án khác</span>
                                        </a>
                                    </div>
                                </div>
                            </div>


                            <div class="testing-exercise    " index="2f" typequestion="guessing" time="15" type="2" style="display: none;">

                                <div class="testing-exercise-flashcard">

                                    <div class="flash-card-viewport">
                                        <div class="flash-card-box-1" id="learning-theory-box-1" index="1">

                                            <figure class="slide-1 jbs-current-1">
                                                <div class="testing-exercise-content">
                                                    <p class="testing-exercise-title">Gõ từ phù hợp:</p>
                                                    <div class="testing-exercise-quesiton">

                                                        <p class="question-image"><img src="http://www.voca.vn/assets/assets-v2/img/library/congruent.jpg" style="height: 160px;"></p>
                                                        <p class="question-word-img">độ lệch</p>
                                                    </div>

                                                </div>
                                            </figure>


                                        </div>
                                    </div>

                                </div>

                                <div class="testing-exercise-answer ">
                                    <div class="answer-cover">
                                        <div class="testing-exercise-answer answer-writing">
                                            <div class="answer-cover">
                                                <input class="answer-writing" placeholder="Gõ đáp án của bạn tại đây" name="">
                                                <a class="answer-check" href="javascript:void(0)">Kiểm tra</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="testing-process">
                        <span class="testing-process-bar learning-main-process-value" style="width: 5.55556%;"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

    <!-- Bootstrap js -->

</body>
<script type="text/javascript">
    // $(document).ready(function() {

    //     window.history.forward();

    //     $('.button-exit').click(function() {

    //         var msg = $('#back-link').attr('msg');
    //         var link = $('#back-link').val();
    //         if (confirm(msg)) {
    //             window.location.href = link;
    //         }
    //     });
    //     $('.btn-ready').click(function() {
    //         runCountdown();
    //         $('.testing-rule').fadeOut(function() {
    //             updateProcess();
    //         });
    //     });
    // });
    var parent = $('#hidden-parent').val();
    var parentSlug = $('#hidden-parent-name').val();
    var topicSlug = $('#hidden-topic-name').val();
    var dataAnswer = {};
    dataAnswer.topicId = $('#hidden-topicid').val();
    dataAnswer.total = parseInt($('.testing-body-cover').attr('total'));
    dataAnswer.answerGuess = [];
    dataAnswer.answerWrite = [];
    // CHECK ANSWER GUESSING
    $(document).on('click', '.testing-exercise-answer ', function() {
        // Get element
        var eleAnswer = $(this);
        var divParent = $(this).parents('.testing-exercise');

        stopCountdown();

        // Check Show answer
        if (!$(divParent).hasClass('show-answer')) {
            // Add class show answer
            $(divParent).addClass('show-answer');

            // Get value for ajax
            var token = $(divParent).attr('token');
            var answer = $(eleAnswer).attr('val');

            // Get answer true
            // checkTestingAnswer(answer, function(result) {
            //     console.log(result);
            //     var check = 0;

            //     if (result[0] == 1) {
            //         check = 1;
            //     }

            //     // Check answer
            //     if (check == 1) {
            //         $(eleAnswer).addClass('choose-right');
            //         soundAnswerRight();

            //     } else {
            //         $(eleAnswer).addClass('choose-wrong');
            //         $(divParent).find('.testing-exercise-answer .answer-guessing[val="' + result[1] + '"]').addClass('choose-right');
            //         soundAnswerWrong();
            //     }

            //     if ($(divParent).find('.testing-exercise-answer .answer-guessing.choose-right[hideAnswer] .guessing-text').length > 0) {
            //         $(divParent).find('.testing-exercise-answer .answer-guessing.choose-right[hideAnswer] .guessing-text').text($(divParent).find('.testing-exercise-answer .answer-guessing.choose-right[hideAnswer]').attr('hideAnswer'));
            //     }
            //     dataAnswer.answerGuess.push({
            //         pos: answer,
            //         card: $('.exercise-current').attr('index'),
            //         word: $('.exercise-current .testing-exercise-result').attr('word'),
            //         synset: $('.exercise-current .testing-exercise-result').attr('synset'),
            //         answerA: $('.exercise-current').find('.answer-guessing[val="1"] .guessing-text').text().trim(),
            //         answerB: $('.exercise-current').find('.answer-guessing[val="2"] .guessing-text').text().trim(),
            //         answerC: $('.exercise-current').find('.answer-guessing[val="3"] .guessing-text').text().trim(),
            //         answerD: $('.exercise-current').find('.answer-guessing[val="4"]').attr('hideanswer').trim(),
            //         time: Math.max(totalTime - second + 1, 1),
            //         right: result[1],
            //         isRight: check
            //     });
            //     console.log(dataAnswer);
            //     // Show answer
            //     showAnswer(divParent);



            // });

        }
    });
    $('.answer-guessing').click(function(e) {
        e.preventDefault();
        $('')

    });

    // CHECK ANSWER WRITING
    $(document).on('click', '.testing-exercise-answer .answer-check.check-active', function() {

        var divParent = $(this).parents(".testing-exercise");
        var button = $(this);
        stopCountdown();

        if (!$(divParent).hasClass('show-answer')) {

            $(divParent).addClass('show-answer');

            $(button).removeClass('check-active');

            var inputAnswer = $(divParent).find('.testing-exercise-answer .answer-writing');
            var userAnswer = $(inputAnswer).val().trim().toLowerCase();
            var token = $(divParent).attr('token');
            $(inputAnswer).trigger('blur');

            // Get answer true
            checkTestingAnswer(userAnswer, function(result) {
                trueAnswer = String(result).trim().toLowerCase();
                var check = 0;
                // RIGHT
                if (result[0] == 1) {
                    check = 1;
                    soundAnswerRight();
                    $(inputAnswer).addClass('answer-right');

                    // WRONG
                } else {
                    soundAnswerWrong();
                    $(inputAnswer).addClass('answer-wrong');
                    $(inputAnswer).val(result[1]);

                }
                dataAnswer.answerWrite.push({
                    answer: userAnswer,
                    card: $('.exercise-current').attr('index'),
                    word: $('.exercise-current .testing-exercise-result').attr('word'),
                    synset: $('.exercise-current .testing-exercise-result').attr('synset'),
                    time: Math.max(totalTime - second + 1, 1),
                    right: result[1],
                    isRight: check
                });

                // Show answer
                showAnswer(divParent);

            });
        }


    });
    $(document).on('keyup', '.testing-exercise-answer .answer-writing', function(e) {
        if (e.keyCode == 13) {
            $(this).next(".answer-check").trigger('click');
        } else {
            if ($(this).val().length > 0) {
                $(this).next(".answer-check").addClass('check-active');
            } else {
                $(this).next(".answer-check").removeClass('check-active');
            }
        }
    });
    $(document).on('keydown', '.testing-exercise-answer .answer-writing', function(e) {
        if (e.keyCode == 13) {
            return false;
        }
    });

    function endTime() {
        readyRunCountdown = 1;
        var divParent = $(".testing-exercise.exercise-current");
        var typeQuestion = $(divParent).attr('typeQuestion');

        var token = $(divParent).attr('token');

        if (!$(divParent).hasClass('show-answer')) {

            $(divParent).addClass('show-answer');

            // GUESSING
            if (typeQuestion == "guessing") {

                checkTestingAnswer(0, function(result) {

                    $(divParent).find('.testing-exercise-answer .answer-guessing[val="' + result[1] + '"]').addClass('choose-right');
                    soundAnswerWrong();

                    if ($(divParent).find('.testing-exercise-answer .answer-guessing.choose-right[hideAnswer] .guessing-text').length > 0) {
                        $(divParent).find('.testing-exercise-answer .answer-guessing.choose-right[hideAnswer] .guessing-text').text($(divParent).find('.testing-exercise-answer .answer-guessing.choose-right[hideAnswer]').attr('hideAnswer'));
                    }

                    dataAnswer.answerGuess.push({
                        pos: 0,
                        card: $('.exercise-current').attr('index'),
                        word: $('.exercise-current .testing-exercise-result').attr('word'),
                        synset: $('.exercise-current .testing-exercise-result').attr('synset'),
                        answerA: $('.exercise-current').find('.answer-guessing[val="1"] .guessing-text').text().trim(),
                        answerB: $('.exercise-current').find('.answer-guessing[val="2"] .guessing-text').text().trim(),
                        answerC: $('.exercise-current').find('.answer-guessing[val="3"] .guessing-text').text().trim(),
                        answerD: $('.exercise-current').find('.answer-guessing[val="4"]').attr('hideanswer').trim(),
                        time: totalTime,
                        right: result[1],
                        isRight: 0
                    });

                    // Show answer
                    showAnswer(divParent);

                });

                // WRITING
            } else {
                var inputAnswer = $(divParent).find('.testing-exercise-answer .answer-writing');
                $(inputAnswer).trigger('blur');

                // Get answer true
                checkTestingAnswer("no answer", function(result) {

                    soundAnswerWrong();
                    $(inputAnswer).addClass('answer-wrong');
                    $(inputAnswer).val(result[1]);


                    dataAnswer.answerWrite.push({
                        answer: "no answer",
                        card: $('.exercise-current').attr('index'),
                        word: $('.exercise-current .testing-exercise-result').attr('word'),
                        synset: $('.exercise-current .testing-exercise-result').attr('synset'),
                        time: totalTime,
                        right: result[1],
                        isRight: 0
                    });
                    // Show answer
                    showAnswer(divParent);

                });

            }

        }
    }

    function showAnswer(divParent) {

        setTimeout(function() {

            $(divParent).find('.flash-card-box').boxSlider('showSlide', 1);

            $('.flash-card-box').boxSlider('option', 'onafter', function($previousSlide, $currentSlide, currIndex, nextIndex) {
                if (nextIndex == 1) {

                    if ($(divParent).find('.testing-exercise-result .result-content-sound').length > 0) {
                        playSound($(divParent).find('.testing-exercise-result .result-content-sound'), function() {
                            nextQuestion();
                        });
                        setTimeout(function() {
                            nextQuestion();
                        }, 2000);
                    } else {
                        nextQuestion();
                    }
                }
            });
        }, 1000);

        setTimeout(function() {
            // $("#testing-countdown").fadeOut(400);
        }, 500);
    }

    function nextQuestion() {

        setTimeout(function() {

            var divCurrent = $(".testing-exercise.exercise-current");
            var indexCur = parseInt($(divCurrent).attr('index'));
            var divNext = $(".testing-exercise[index='" + (indexCur + 1) + "']");

            if (divNext.length > 0) {
                $(divCurrent).removeClass('exercise-current');
                $(divNext).addClass('exercise-current');

                $(divCurrent).addClass('learning-hide-item');
                $(divNext).addClass('learning-show-item');

                setTimeout(function() {

                    $(divCurrent).css('display', 'none');
                }, 500);

                updateProcess();
            } else {

                $.blockUI({
                    message: vocaLang.pleaseWaitVoca,
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '2px',
                        '-moz-border-radius': '2px',
                        opacity: .5,
                        color: '#fff'
                    }
                });

                document.cookie = 'userActive=; expires=Thu, 01-Jan-70 00:00:01 GMT;';
                dataAnswer = JSON.stringify(dataAnswer);
                $.ajax({
                    url: urlConfig('edit-test-result'),
                    data: {
                        dataAnswer: dataAnswer
                    },
                    dataType: 'text',
                    type: "POST",
                    //async: false
                }).success(function(result) {

                    if (result != 1) {
                        location.reload();
                    } else {
                        window.location.href = urlConfig(parentSlug + '/' + topicSlug);
                        window.onbeforeunload = null;
                    }
                }).error(function(jqXHR, textStatus, errorThrown) {

                });

                // setTimeout(function() {
                //     //show charts for test
                //     window.location.href = urlConfig(parentSlug + '/' + topicSlug);
                //     window.onbeforeunload = null;
                // }, 3000);

            }
        }, 0);

    }

    function updateProcess() {
        $('.testing-answer-group').fadeOut();
        var total = parseInt($(".testing-body-cover").attr('total'));
        var indexCur = parseInt($(".testing-exercise.exercise-current").attr('index'));
        if (total > 0) {
            $(".testing-process-bar").width((indexCur + 1) * 100 / total + '%');
        }
        totalTime = parseInt($('.exercise-current').attr('time'));
        $(".testing-countdown svg circle.circle").css('stroke-dashoffset', '0px');
        $(".testing-countdown-number").text(totalTime);
        $(".testing-exercise.exercise-current").css('display', 'block');;

        readyAnswer = 1;
        if ($(".testing-exercise.exercise-current .testing-exercise-answer .answer-writing").length > 0) {
            $(".testing-exercise.exercise-current .testing-exercise-answer .answer-writing").focus();
        }
        setTimeout(function() {
            // $("#testing-countdown").fadeIn(400);
            if ($(".testing-exercise.exercise-current .testing-exercise-quesiton .question-sound.main-sound-play-auto").length > 0) {
                playSound($('.testing-exercise.exercise-current .testing-exercise-quesiton .question-sound'), function() {

                    runCountdown();
                });
            } else {

                runCountdown();
            }

        }, 500);
        updateImage(indexCur);
    }


    // Time
    var t;
    var totalTime = 15;
    var second = totalTime;

    var readyRunCountdown = 1;
    var readyAnswer = 1;

    function runCountdown() {
        if (readyRunCountdown == 1 && readyAnswer == 1) {
            readyRunCountdown = 0;
            second = totalTime;

            var runCircle = 0;

            t = window.setInterval(function() {
                if (window.blurred) {

                } else {

                    second = --second <= 0 ? 0 : second;

                    if (runCircle == 0) {
                        runCircle = 1;
                        $(".testing-countdown svg circle.circle").animate({
                            "stroke-dashoffset": "-101px",
                        }, (second) * 1000, "linear", function() {

                        });
                    }

                    $(".testing-countdown-number").text(second);
                    if (second == 0) {
                        window.clearInterval(t);
                        endTime();
                        return false;
                    }

                    if (second <= 5) {
                        // $(".testing-theory-time-countdown").addClass("end-time");
                        playWarningTimeSound();
                    } else {
                        playNormalTimeSound();
                    }
                }

            }, 1000);
        }
    }

    function stopCountdown() {
        $(".testing-countdown svg circle.circle").stop(true);
        window.clearInterval(t);
        readyRunCountdown = 1;
        readyAnswer = 0;
    }

    window.onblur = function() {
        window.blurred = true;
        $(".testing-countdown svg circle.circle").stop(true);
    };
    window.onfocus = function() {
        window.blurred = false;
        if (readyRunCountdown == 0) {
            $(".testing-countdown svg circle.circle").animate({
                "stroke-dashoffset": "-101px"
            }, (second) * 1000, "linear");
        }
    };

    function playNormalTimeSound() {
        if ($("#normal-time-sound").length > 0) {
            $("#normal-time-sound")[0].load();
            $("#normal-time-sound")[0].onloadeddata = function() {
                $("#normal-time-sound")[0].play();
            };
        }
    }

    function playWarningTimeSound() {
        if ($("#warning-time-sound").length > 0) {
            $("#warning-time-sound")[0].load();
            $("#warning-time-sound")[0].onloadeddata = function() {
                $("#warning-time-sound")[0].play();
            };
        }
    }

    // function checkTestingAnswer(answer, getAnswer) {

    //     $.ajax({

    //         url: urlConfig('check-answer'),
    //         data: {
    //             'card': $('.exercise-current').attr('index'),
    //             'answer': answer
    //         },
    //         type: "POST"
    //     }).success(function(result) {
    //         getAnswer(result);
    //     });

    // }




    function updateImage(indexCurrent) {
        var next_1 = $(".testing-exercise[index='" + indexCurrent + "']");
        var next_2 = $(".testing-exercise[index='" + (indexCurrent + 1) + "']");

        if (next_1.length > 0) {
            if ($(next_1).find('.testing-exercise-result .result-img img').attr('src') == '' || $(next_1).find('.testing-exercise-result .result-img img').attr('src') == undefined) {
                $(next_1).find('.testing-exercise-result .result-img img').attr('src', $(next_1).find(".testing-exercise-result").attr('img_url'));
            }
        }

        if (next_2.length > 0) {
            if ($(next_2).find('.testing-exercise-result .result-img img').attr('src') == '' || $(next_2).find('.testing-exercise-result .result-img img').attr('src') == undefined) {
                $(next_2).find('.testing-exercise-result .result-img img').attr('src', $(next_2).find(".testing-exercise-result").attr('img_url'));
            }
        }


    }

    $(document).ready(function() {
        $(".testing-exercise-flashcard .flash-card-box").each(function() {
            var index = $(this).attr('index');
            var defaultOptions = {
                speed: 600,
                timeout: 300,
                next: '#next',
                prev: '#prev',
                pause: '#pause'
            };
            $("#learning-theory-box-" + (index)).boxSlider(defaultOptions);
        });
    });
    $(document).ready(function() {
        $('.answer-guessing').click(function(e) {
            e.preventDefault();
            $('.testing-exercise.exercise-current').addclass('learning-hide-item');

        });
    });
    // $('.answer-guessing').click(function (e) { 
    //     e.preventDefault();
    .addclass('show-answer');
    //     $('.testing-exercise.exercise-current').addclass('learning-hide-item');

    // });
</script>

</html>