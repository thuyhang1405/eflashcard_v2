<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *Must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title -->
    <title>eFlashcard | Home</title>

    <!-- Favicon -->
    <link rel="icon" href="img/core-img/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=DM+Sans:300,400,700|Indie+Flower" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

    <!-- Latest compiled JavaScript -->

    <link rel="stylesheet" href="fonts/icomoon/style.css">
    <!-- Stylesheet -->
    <link rel="stylesheet" href="css/style.css">



</head>

<body>
    <!-- Preloader -->
    <div id="preloader">
        <div class="spinner"></div>
    </div>

    <!-- ##### Header Area Start ##### -->
    <header class="header-area">

        <!-- Top Header Area -->


        <!-- Navbar Area -->
        <div class="clever-main-menu">
            <div class="classy-nav-container breakpoint-off">
                <!-- Menu -->
                <nav class="classy-navbar justify-content-between" id="cleverNav">

                    <!-- Logo -->
                    <a class="nav-brand" href="index.html">
                        <p>eFlashcard</p>
                    </a>

                    <!-- Navbar Toggler -->
                    <div class="classy-navbar-toggler">
                        <span class="navbarToggler"><span></span><span></span><span></span></span>
                    </div>

                    <!-- Menu -->
                    <div class="classy-menu">

                        <!-- Close Button -->
                        <div class="classycloseIcon">
                            <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                        </div>

                        <!-- Nav Start -->
                        <div class="classynav">
                            <ul>
                                <li><a href="/">Home</a></li>
                                <li><a href="/courses">Courses</a></li>
                                <li><a href="/packages">Packages</a></li>
                                <li><a href="#member">Members</a></li>
                                <li><a href="#contact">Contact</a></li>
                            </ul>

                            <!-- Search Button -->
                            <div class="search-area">
                                <form action="#" method="post">
                                    <input type="search" name="search" id="search" placeholder="Search">
                                    <button type="submit"><i class="fas fa-search" style="margin-top: 0px;" aria-hidden="true"></i></button>
                                </form>
                            </div>

                            <!-- Register / Login -->
                            <div class="register-login-area">
                                
                                <a href="/login" class="btn active">Login</a>
                            </div>

                        </div>
                        
                        <!-- Nav End -->
                    </div>
                </nav>
            </div>
        </div>
    </header>
    <!-- ##### Header Area End ##### -->

    <!-- ##### Hero Area Start ##### -->
    <section class="hero-area bg-img bg-overlay-2by5" style="background-image: url(img/bg-img/bg1.jpg); background-size:1520px ; height: 675px; background-position-y: 80px;">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <!-- Hero Content -->
                    <div class="hero-content text-center">
                        <h2>Learn English the eFlashcard's way</h2>
                        <a href="register.html" class="btn clever-btn">REGISTER NOW</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ##### Hero Area End ##### -->

    <!-- ##### Cool Facts Area Start ##### -->
    <section class="cool-facts-area section-padding-100-0">
        <div class="container">
            <div class="row">
                <div class="col-12 m-auto">
                    <div class="container">
                        <div class="row">
                            <!-- Single Cool Facts Area -->
                            <div class="col-12 col-sm-6 col-lg-4 Participants">
                                <div class="single-cool-facts-area text-center mb-100 wow fadeInUp" data-wow-delay="250ms" style="font-size: 28px">
                                    <div class="icon">
                                        <img src="img/core-img/docs.jpg" alt="">
                                    </div>
                                    <h2><span class="counter">1912</span></h2>
                                    <p style="color:white; font-family:serif; font-weight:bold">Participants</p>
                                </div>
                            </div>

                            <!-- Single Cool Facts Area -->
                            <div class="col-12 col-sm-6 col-lg-4 Topics">
                                <div class="single-cool-facts-area text-center mb-100 wow fadeInUp" data-wow-delay="500ms" style="font-size: 28px">
                                    <div class="icon">
                                        <img src="img/core-img/star.jpg" alt="">
                                    </div>
                                    <h2><span class="counter">123</span></h2>
                                    <p style="color:white; font-family:serif; font-weight:bold">Topics</p>
                                </div>
                            </div>

                            <!-- Single Cool Facts Area -->
                            <div class="col-12 col-sm-6 col-lg-4 Flashcard" >
                                <div class="single-cool-facts-area text-center mb-100 wow fadeInUp" data-wow-delay="750ms" style="font-size: 28px" >
                                    <div class="icon">
                                        <img src="img/core-img/events.jpg" alt="">
                                    </div>
                                    <h2><span class="counter">89</span></h2>
                                    <p style="color:white; font-family:serif; font-weight:bold">Available Flashcard</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <!-- Single Cool Facts Area -->

            </div>
        </div>
    </section>
    <!-- ##### Cool Facts Area End ##### -->

    <!-- ##### Popular Courses Start ##### -->
    <section class="popular-courses-area section-padding-100-0" style="background-image: url(img/core-img/texture.png);">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-heading">
                        <h3>Popular Topic Flashcard</h3>
                    </div>
                </div>
            </div>

            <div class="row">
                <!-- Single Popular Course -->
                <div class="col-12 col-md-6 col-lg-4">
                    <a>
                        <div class="single-popular-course mb-100 wow fadeInUp" data-wow-delay="250ms">
                            <a href="#"><img src="img/bg-img/c1.jpg" alt=""></a>

                            <!-- Course Content -->
                            <div class="course-content">
                                <a href="#">
                                    <h4>The Family</h4>
                                </a>
                                <div class="meta d-flex align-items-center">
                                    <a href="#">Sarah Parker</a>
                                    <span><i class="fa fa-circle" aria-hidden="true"></i></span>
                                    <a href="#">Art &amp; Design</a>
                                </div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce enim nulla, mollis eu metus in, sagittis</p>
                            </div>
                            <!-- Seat Rating Fee -->
                            <div class="seat-rating-fee d-flex justify-content-between">
                                <div class="course-fee h-100">
                                    <a href="#" class="free">Free</a>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <!-- Single Popular Course -->
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="single-popular-course mb-100 wow fadeInUp" data-wow-delay="500ms">
                        <a href="#"><img src="img/bg-img/c2.jpg" alt=""></a>
                        <!-- Course Content -->
                        <div class="course-content">
                            <a href="#">
                                <h4>Vegetables</h4>
                            </a>
                            <div class="meta d-flex align-items-center">
                                <a href="#">Sarah Parker</a>
                                <span><i class="fa fa-circle" aria-hidden="true"></i></span>
                                <a href="#">Art &amp; Design</a>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce enim nulla, mollis eu metus in, sagittis</p>
                        </div>
                        <!-- Seat Rating Fee -->
                        <div class="seat-rating-fee d-flex justify-content-between">
                            <div class="course-fee h-100">
                                <a href="#">Learn Now</a>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Single Popular Course -->
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="single-popular-course mb-100 wow fadeInUp" data-wow-delay="750ms">
                        <a href="#"><img src="img/bg-img/c3.jpg" alt=""></a>
                        <!-- Course Content -->
                        <div class="course-content">
                            <a href="">
                                <h4>Everyday Clothes</h4>
                            </a>
                            <div class="meta d-flex align-items-center">
                                <a href="#">Sarah Parker</a>
                                <span><i class="fa fa-circle" aria-hidden="true"></i></span>
                                <a href="#">Art &amp; Design</a>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce enim nulla, mollis eu metus in, sagittis</p>
                        </div>
                        <!-- Seat Rating Fee -->
                        <div class="seat-rating-fee d-flex justify-content-between">

                            <div class="course-fee h-100">
                                <a href="#">Learn Now</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-6 col-lg-4">
                    <div class="single-popular-course mb-100 wow fadeInUp" data-wow-delay="750ms">
                        <a href="#"><img src="img/bg-img/c4.jpg" alt=""></a>
                        <!-- Course Content -->
                        <div class="course-content">
                            <a href="">
                                <h4>Houses</h4>
                            </a>
                            <div class="meta d-flex align-items-center">
                                <a href="#">Sarah Parker</a>
                                <span><i class="fa fa-circle" aria-hidden="true"></i></span>
                                <a href="#">Art &amp; Design</a>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce enim nulla, mollis eu metus in, sagittis</p>
                        </div>
                        <!-- Seat Rating Fee -->
                        <div class="seat-rating-fee d-flex justify-content-between">

                            <div class="course-fee h-100">
                                <a href="#">Learn Now</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-6 col-lg-4">
                    <div class="single-popular-course mb-100 wow fadeInUp" data-wow-delay="750ms">
                        <a href="#"><img src="img/bg-img/c5.jpg" alt=""></a>
                        <!-- Course Content -->
                        <div class="course-content">
                            <a href="">
                                <h4>Medical and Dental Care</h4>
                            </a>
                            <div class="meta d-flex align-items-center">
                                <a href="#">Sarah Parker</a>
                                <span><i class="fa fa-circle" aria-hidden="true"></i></span>
                                <a href="#">Art &amp; Design</a>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce enim nulla, mollis eu metus in, sagittis</p>
                        </div>
                        <!-- Seat Rating Fee -->
                        <div class="seat-rating-fee d-flex justify-content-between">

                            <div class="course-fee h-100">
                                <a href="#">Learn Now</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-6 col-lg-4">
                    <div class="single-popular-course mb-100 wow fadeInUp" data-wow-delay="750ms">
                        <a href="#"><img src="img/bg-img/c6.jpg" alt=""></a>
                        <!-- Course Content -->
                        <div class="course-content">
                            <a href="">
                                <h4>Car</h4>
                            </a>
                            <div class="meta d-flex align-items-center">
                                <a href="#">Sarah Parker</a>
                                <span><i class="fa fa-circle" aria-hidden="true"></i></span>
                                <a href="#">Art &amp; Design</a>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce enim nulla, mollis eu metus in, sagittis</p>
                        </div>
                        <!-- Seat Rating Fee -->
                        <div class="seat-rating-fee d-flex justify-content-between">
                            <div class="course-fee h-100">
                                <a href="#">Learn Now</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ##### Popular Courses End ##### -->
    <div class="site-section" id='pricing'>
        <div class="container">
            <div class="row">
                <div class="col-md-4-a">

                    <h2 class="text-black">Our Pricing</h2>
                    <h3>Gói sử dụng</h3>
                </div>
                <div class="col-md-4-b">
                    <div class="pricing teal">
                        <span class="price">
                      <span>$30</span>
                        </span>
                        <h3>Silver Pack</h3>
                        <ul class="ul-check ">

                            <li><i class="far fa-check-square"></i> Sử dụng 1 năm</li>
                            <li><i class="far fa-check-square"></i> Hơn 100 chủ đề</li>
                            <li><i class="far fa-check-square"></i> Từ vừng gần gũi, thiết thực</li>
                        </ul>
                        <p><a href="/payment-step-1" class="btn btn-teal btn-custom-1 mt-4">Buy Now</a></p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="pricing danger">
                        <span class="price">
                      <span>$100</span>
                        </span>
                        <h3>Golden Pack</h3>
                        <ul class="ul-check list-unstyled danger">
                            <li><i class="far fa-check-square"></i> Sử dụng trọn đời</li>
                            <li><i class="far fa-check-square"></i> Hơn 250 chủ đề</li>
                            <li><i class="far fa-check-square"></i> Từ vừng gần gũi, thiết thực</li>
                        </ul>
                        <p> <a href="#" class="btn btn-danger btn-custom-1 mt-4">Buy Now</a></p>
                    </div>
                </div>
            </div>
            <div>
                <p class="showmore">
                    <a href="/packages" class="btn readmore"><i class="fas fa-forward"></i> Show more</a></p>
            </div>
        </div>
    </div>
    <!-- ##### Best Tutors Start ##### -->
    <section id='member' class="best-tutors-area section-padding-100">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-heading">
                        <h3>Members</h3>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="tutors-slide owl-carousel wow fadeInUp" data-wow-delay="250ms">

                        <!-- Single Tutors Slide -->
                        <div class="single-tutors-slides">
                            <!-- Tutor Thumbnail -->
                            <div class="tutor-thumbnail">
                                <img src="img/bg-img/t1.jpg" alt="">
                            </div>
                            <!-- Tutor Information -->
                            <div class="tutor-information text-center">
                                <h5>Tô Thúy Hằng</h5>
                                <span>Student</span>
                                <p>MSSV: 17520438</p>
                                <div class="social-info">
                                    <a href="https://www.facebook.com/hutieu1405"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                    <a href="https://www.instagram.com/bu6131/"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                    <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>

                        <!-- Single Tutors Slide -->
                        <div class="single-tutors-slides">
                            <!-- Tutor Thumbnail -->
                            <div class="tutor-thumbnail">
                                <img src="img/bg-img/t2.jpg" alt="">
                            </div>
                            <!-- Tutor Information -->
                            <div class="tutor-information text-center">
                                <h5>Cao Thiện Huân</h5>
                                <span>Student</span>
                                <p>MSSV: 17520527</p>
                                <div class="social-info">
                                    <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                    <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                    <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>

                        <div class="single-tutors-slides">
                            <!-- Tutor Thumbnail -->
                            <div class="tutor-thumbnail">
                                <img src="img/bg-img/t4.jpg" alt="">
                            </div>
                            <!-- Tutor Information -->
                            <div class="tutor-information text-center">
                                <h5>Hồ Thị Ngọc Huyền</h5>
                                <span>Student</span>
                                <p>MSSV: 17520596</p>
                                <div class="social-info">
                                    <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                    <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                    <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>

                        <!-- Single Tutors Slide -->
                        <div class="single-tutors-slides">
                            <!-- Tutor Thumbnail -->
                            <div class="tutor-thumbnail">
                                <img src="img/bg-img/t3.jpg" alt="">
                            </div>
                            <!-- Tutor Information -->
                            <div class="tutor-information text-center">
                                <h5>Mai Xuân Hùng</h5>
                                <span>Thạc Sĩ</span>
                                <p>Giảng viên Khoa HTTT</p>
                                <div class="social-info">
                                    <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                    <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                    <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ##### Best Tutors End ##### -->

    <!-- ##### Register Now Start ##### -->
    <section class="register-now section-padding-100-0 d-flex justify-content-between align-items-center" style="background-image: url(img/core-img/texture.png);">
        <!-- Register Contact Form -->
        <div class="register-contact-form mb-100 wow fadeInUp" data-wow-delay="250ms">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="forms">
                            <h4>Register Now</h4>
                            <form action="#" method="post">
                                <div class="row">
                                    <div class="col-12 col-lg-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="text" placeholder="Name">
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-6">
                                        <div class="form-group">
                                            <input type="email" class="form-control" id="email" placeholder="Email">
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="phone" placeholder="Phone">
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="site" placeholder="Address">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <button class="btn clever-btn w-100">Send Message</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Register Now Countdown -->
        <div class="register-now-countdown mb-100 wow fadeInUp" data-wow-delay="500ms">
            <h3>Giảm 20% khi khách hàng mua gói sử dụng từ hôm nay cho đến hết ngày 30/12/2019</h3>
            <h4>Time Remaining</h4>
            <!-- Register Countdown -->
            <div class="register-countdown">
                <div class="events-cd d-flex flex-wrap" data-countdown="2019/11/18"></div>
            </div>
        </div>
    </section>
    <!-- ##### Register Now End ##### -->



    <!-- ##### Footer Area Start ##### -->
    <footer id='contact' class="site-footer">
        <div class="footer-widgets">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-6 col-lg-3">
                        <div class="foot-about">
                            <a class="foot-logo" href="#">
                                <p>eFlashcard</p>
                            </a>

                            <p style="color: burlywood;">Website eFlashcard cung cấp đến học viên đầy đủ các chủ đề. Giúp các học viên tiếp cận cách học tiếng anh hiệu quả nhất.</p>

                            <p class="footer-copyright">
                                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                Copyright &copy;
                                <script>
                                    document.write(new Date().getFullYear());
                                </script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="#" target="_blank">eFlashcard</a>
                                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            </p>
                        </div>
                        <!-- .foot-about -->
                    </div>
                    <!-- .col -->

                    <div class="col-12 col-md-6 col-lg-3 mt-5 mt-md-0">
                        <div class="foot-contact">
                            <h2>Contact Us</h2>

                            <ul>
                                <li>Email: info.eflashcard@gmail.com</li>
                                <li>Phone: (+84) 369 791 967</li>
                                <li>Address: Khu phố 6, phường Linh Trung, quận Thủ Đức</li>
                            </ul>
                        </div>
                        <!-- .foot-contact -->
                    </div>
                    <!-- .col -->

                    <div class="col-12 col-md-6 col-lg-3 mt-5 mt-lg-0">
                        <div class="quick-links flex flex-wrap">
                            <h2 class="w-100">Quick Links</h2>

                            <ul class="w-50">
                                <li><a href="courses.html">Courses </a></li>
                                <li><a href="#member">Members </a></li>
                                <li><a href="#contact">Contact Us</a></li>
                            </ul>


                        </div>
                        <!-- .quick-links -->
                    </div>
                    <!-- .col -->

                    <div class="col-12 col-md-6 col-lg-3 mt-5 mt-lg-0">
                        <div class="follow-us">
                            <h2>Follow Us</h2>

                            <ul class="follow-us flex flex-wrap align-items-center">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            </ul>
                        </div>
                        <!-- .quick-links -->
                    </div>
                    <!-- .col -->
                </div>
                <!-- .row -->
            </div>
            <!-- .container -->
        </div>
        <!-- .footer-widgets -->


        <!-- .footer-bar -->
    </footer>
    <!-- .site-footer -->
    <!-- ##### Footer Area End ##### -->

    <!-- ##### All Javascript Script ##### -->
    <!-- jQuery-2.2.4 js -->
    <script src="js/jquery/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <script src="js/bootstrap/popper.min.js"></script>
    <!-- Bootstrap js -->
    <script src="js/bootstrap/bootstrap.min.js"></script>
    <!-- All Plugins js -->
    <script src="js/plugins/plugins.js"></script>
    <!-- Active js -->
    <script src="js/active.js"></script>
    <script src="js/bootstrap-datepicker.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <script src="fonts/fontawesome-webfont.woff"></script>


</body>

</html>