<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *Must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title -->
    <title>Introdution</title>

    <!-- Favicon -->
    <link rel="icon" href="img/core-img/favicon.ico">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <!-- Stylesheet -->
    <link rel="stylesheet" href="style.css">

</head>

<body>
    <div id="voca-wrap">
        <div class="main-wrapper">
            <div class="learning-main-header promotion-main-header">
                <div class="header-cover">
                    <div class="header-left">
                        <a href="#" class="button-back">
                            <i class="fas fa-backspace"></i> Back</a>
                    </div>
                    <div class="header-right"></div>
                    <div class="header-logo">
                        <a href="">
                            <p>eFlashcard</p>
                        </a>
                        
                    </div>
                    <div class="header-right">
                                <div class="header-avatar">
                                    <div class="dropdown">
                                        <button type="button">
                                            <a href="">
                                                <img src="img/img_2.jpg" alt="">
                                            </a>
                                        </button>
                                    </div>
                                </div>
                            </div>
                </div>
            </div>
            <div class="learning-main-body">
                <div class="layout-1 intro-topic-container ">
                    <p class="intro-topic-title">Chào mừng bạn đến với chủ đề</p>
                    <div class="div-intro-topic-name">
                        <p class="intro-topic-name ">Family</p>
                    </div>

                    <p class="intro-topic-line"></p>
                    <p class="intro-topic-text ">Quy trình học gồm 3 bước</p>
                    <div class="intro-topic-step ">
                        <div class="step-item " style="background-color: #ffeb3b ">

                            <p class="step-number ">Bước 1</p>
                            <p class="step-title ">Sàng lọc</p>
                            <p class="step-text ">Giúp xác định rõ mục tiêu cần học, tránh lãng phí thời gian với những từ đã biết.</p>
                        </div>

                        <div class="step-item " style="background-color:#c25fdb ">

                            <p class="step-number ">Bước 2</p>
                            <p class="step-title ">Học từ vựng</p>
                            <p class="step-text ">Giúp xác định rõ mục tiêu cần học, tránh lãng phí thời gian với những từ đã biết.</p>
                        </div>

                        <div class="step-item " style="background-color:#7af3f3 ">

                            <p class="step-number ">Bước 3</p>
                            <p class="step-title ">Kiểm tra</p>
                            <p class="step-text ">Giúp xác định rõ mục tiêu cần học, tránh lãng phí thời gian với những từ đã biết.</p>
                        </div>
                    </div>
                    <p class="intro-topic-bot-text">Bạn đã sẳn sàng chưa?</p>
                    <a class="intro-topic-button-cover">
                        <div class="btn-start">
                            Bắt đầu học
                        </div>
                    </a>

                </div>
                <div class="learning-tab">
                    <div class="learning-tab-content">
                        <a href="" class="tab-link tab-link-selecting tab-link-active">
                            <span>Bước 1: Sàng lọc</span>
                        </a>
                        <a href="" class="tab-link button-learning-now tab-link-learning tab-link-disable">
                            <span>Bước 2: Học</span>
                        </a>
                        <a href="" class="tab-link tab-link-testing tab-link-disable">
                            <span>Bước 3: Kiểm tra</span>
                        </a>
                    </div>
                </div>
                <div class="layout-2">
                    <div class="filter-word-container">
                        <div class="filter-word-body">
                            <div id="learning-theory-kontext" class="kontext capable">
                                <div class="layer show item-current" wordid="400523115">
                                    <div class="flash-card-viewport">
                                        <div class="flash-card-box" id="learning-theory-box-1" index="1">
                                            <figure class="slide jbs-current-1">
                                                <div class="learning-theory-flash-card-content">
                                                    <div class="item-content">
                                                        <div class="item-content-cover">
                                                            <p class="item-title"> Bạn biết từ vựng này hay không?</p>
                                                            <a class="item-sound main-sound-play" sound_url="https://www.voca.vn/assets/sounds/s5q7obufvngmv3dyhf070fqkep3brrojr9seactde.mp3" style="background: url(img/icons/speaker.png) 0px -165px; background-size: 78px;
                                                            padding-left: 60px;
                                                            margin-left: 10px;"></a>

                                                            <p class="item-name">resource</p>
                                                            <p class="item-type">(n)</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="dimmer"></div>
                                </div>
                                <div class="layer" wordid="400523116">
                                    <div class="flash-card-viewport">
                                        <div class="flash-card-box" id="learning-theory-box-2" index="2">
                                            <figure class="slide jbs-current-1">
                                                <div class="learning-theory-flash-card-content">
                                                    <div class="item-content">
                                                        <div class="item-content-cover">
                                                            <p class="item-title"> Bạn biết từ vựng này hay không?</p>
                                                            <a class="item-sound main-sound-play" sound_url="https://www.voca.vn/assets/sounds/9cfozkydeogtwoqpdp4khipgvq78xgi0ajman6ecle.mp3" style="background: url(img/icons/speaker.png) 0px -165px; background-size: 78px;
                                                            padding-left: 60px;
                                                            margin-left: 10px;"></a>
                                                            <p class="item-name">mother</p>
                                                            <p class="item-type">(n)</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </figure>
                                        </div>
                                    </div>

                                </div>
                                <div class="layer" wordid="">
                                    <div class="flash-card-viewport">
                                        <div class="flash-card-box" id="learning-theory-box-3" index="3">
                                            <figure class="slide jbs-current-1">
                                                <div class="learning-theory-flash-card-content">
                                                    <div class="item-content">
                                                        <div class="item-content-cover">
                                                            <p class="item-title"> Bạn biết từ vựng này hay không?</p>
                                                            <a class="item-sound main-sound-play" sound_url="https://www.voca.vn/assets/sounds/9cfozkydeogtwoqpdp4khipgvq78xgi0ajman6ecle.mp3" style="background: url(img/icons/speaker.png) 0px -165px; background-size: 78px;
                                                            padding-left: 60px;
                                                            margin-left: 10px;"></a>
                                                            <p class="item-name">father</p>
                                                            <p class="item-type">(n)</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="dimmer"></div>
                                </div>
                                <div class="layer" wordid="">
                                    <div class="flash-card-viewport">
                                        <div class="flash-card-box" id="learning-theory-box-4" index="4">
                                            <figure class="slide jbs-current-1">
                                                <div class="learning-theory-flash-card-content">
                                                    <div class="item-content">
                                                        <div class="item-content-cover">
                                                            <p class="item-title"> Bạn biết từ vựng này hay không?</p>
                                                            <a class="item-sound main-sound-play" sound_url="" style="background: url(img/icons/speaker.png) 0px -165px; background-size: 78px;
                                                            padding-left: 60px;
                                                            margin-left: 10px;"></a>
                                                            <p class="item-name">sister</p>
                                                            <p class="item-type">(n)</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="dimmer"></div>
                                </div>
                                <div class="layer" wordid="">
                                    <div class="flash-card-viewport">
                                        <div class="flash-card-box" id="learning-theory-box-5" index="5">
                                            <figure class="slide jbs-current-1">
                                                <div class="learning-theory-flash-card-content">
                                                    <div class="item-content">
                                                        <div class="item-content-cover">
                                                            <p class="item-title"> Bạn biết từ vựng này hay không?</p>
                                                            <a class="item-sound main-sound-play" sound_url="" style="background: url(img/icons/speaker.png) 0px -165px; background-size: 78px;
                                                            padding-left: 60px;
                                                            margin-left: 10px;"></a>
                                                            <p class="item-name">resource</p>
                                                            <p class="item-type">(n)</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="dimmer"></div>
                                </div>
                                <div class="layer" wordid="">
                                    <div class="flash-card-viewport">
                                        <div class="flash-card-box" id="learning-theory-box-6" index="6">
                                            <figure class="slide jbs-current-1">
                                                <div class="learning-theory-flash-card-content">
                                                    <div class="item-content">
                                                        <div class="item-content-cover">
                                                            <p class="item-title"> Bạn biết từ vựng này hay không?</p>
                                                            <a class="item-sound main-sound-play" sound_url="" style="background: url(img/icons/speaker.png) 0px -165px; background-size: 78px;
                                                            padding-left: 60px;
                                                            margin-left: 10px;"></a>
                                                            <p class="item-name">resource</p>
                                                            <p class="item-type">(n)</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="dimmer"></div>
                                </div>
                                <div class="layer" wordid="">
                                    <div class="flash-card-viewport">
                                        <div class="flash-card-box" id="learning-theory-box-7" index="7">
                                            <figure class="slide jbs-current-1">
                                                <div class="learning-theory-flash-card-content">
                                                    <div class="item-content">
                                                        <div class="item-content-cover">
                                                            <p class="item-title"> Bạn biết từ vựng này hay không?</p>
                                                            <a class="item-sound main-sound-play" sound_url="" style="background: url(img/icons/speaker.png) 0px -165px; background-size: 78px;
                                                            padding-left: 60px;
                                                            margin-left: 10px;" i></a>
                                                            <p class="item-name">resource</p>
                                                            <p class="item-type">(n)</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="dimmer"></div>
                                </div>
                                <div class="layer" wordid="">
                                    <div class="flash-card-viewport">
                                        <div class="flash-card-box" id="learning-theory-box-8" index="8">
                                            <figure class="slide jbs-current-1">
                                                <div class="learning-theory-flash-card-content">
                                                    <div class="item-content">
                                                        <div class="item-content-cover">
                                                            <p class="item-title"> Bạn biết từ vựng này hay không?</p>
                                                            <a class="item-sound main-sound-play" sound_url="" style="background: url(img/icons/speaker.png) 0px -165px; background-size: 78px;
                                                            padding-left: 60px;
                                                            margin-left: 10px;"></a>
                                                            <p class="item-name">resource</p>
                                                            <p class="item-type">(n)</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="dimmer"></div>
                                </div>

                                <div class="layer" wordid="">
                                    <div class="flash-card-viewport">
                                        <div class="flash-card-box" id="learning-theory-box-9" index="9">
                                            <figure class="slide jbs-current-1">
                                                <div class="learning-theory-flash-card-content">
                                                    <div class="item-content">
                                                        <div class="item-content-cover">
                                                            <p class="item-title"> Bạn biết từ vựng này hay không?</p>
                                                            <a class="item-sound main-sound-play" sound_url="" style="background: url(img/icons/speaker.png) 0px -165px; background-size: 78px;
                                                            padding-left: 60px;
                                                            margin-left: 10px;"></a>
                                                            <p class="item-name">resource</p>
                                                            <p class="item-type">(n)</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="dimmer"></div>
                                </div>
                                <div class="layer" wordid="">
                                    <div class="flash-card-viewport">
                                        <div class="flash-card-box" id="learning-theory-box-10" index="10">
                                            <figure class="slide jbs-current-1">
                                                <div class="learning-theory-flash-card-content">
                                                    <div class="item-content">
                                                        <div class="item-content-cover">
                                                            <p class="item-title"> Bạn biết từ vựng này hay không?</p>
                                                            <a class="item-sound main-sound-play" sound_url="" style="background: url(img/icons/speaker.png) 0px -165px; background-size: 78px;
                                                            padding-left: 60px;
                                                            margin-left: 10px;"></a>
                                                            <p class="item-name">resource</p>
                                                            <p class="item-type">(n)</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="dimmer"></div>
                                </div>
                                <div class="layer" wordid="">
                                    <div class="flash-card-viewport">
                                        <div class="flash-card-box" id="learning-theory-box-11" index="11">
                                            <figure class="slide jbs-current-1">
                                                <div class="learning-theory-flash-card-content">
                                                    <div class="item-content">
                                                        <div class="item-content-cover">
                                                            <p class="item-title"> Bạn biết từ vựng này hay không?</p>
                                                            <a class="item-sound main-sound-play" sound_url="" style="background: url(img/icons/speaker.png) 0px -165px; background-size: 78px;
                                                            padding-left: 60px;
                                                            margin-left: 10px;"></a>
                                                            <p class="item-name">resource</p>
                                                            <p class="item-type">(n)</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="dimmer"></div>
                                </div>


                                <div class="layer" wordid="">
                                    <div class="flash-card-viewport">
                                        <div class="flash-card-box" id="learning-theory-box-12" index="12">
                                            <figure class="slide jbs-current-1">
                                                <div class="learning-theory-flash-card-content">
                                                    <div class="item-content">
                                                        <div class="item-content-cover">
                                                            <p class="item-title"> Bạn biết từ vựng này hay không?</p>
                                                            <a class="item-sound main-sound-play" sound_url="" style="background: url(img/icons/speaker.png) 0px -165px; background-size: 78px;
                                                            padding-left: 60px;
                                                            margin-left: 10px;" i></a>
                                                            <p class="item-name">resource</p>
                                                            <p class="item-type">(n)</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="dimmer"></div>
                                </div>
                                <div class="layer" wordid="">
                                    <div class="flash-card-viewport">
                                        <div class="flash-card-box" id="learning-theory-box-13" index="13">
                                            <figure class="slide jbs-current-1">
                                                <div class="learning-theory-flash-card-content">
                                                    <div class="item-content">
                                                        <div class="item-content-cover">
                                                            <p class="item-title"> Bạn biết từ vựng này hay không?</p>
                                                            <a class="item-sound main-sound-play" sound_url="" style="background: url(img/icons/speaker.png) 0px -165px; background-size: 78px;
                                                            padding-left: 60px;
                                                            margin-left: 10px;" i></a>
                                                            <p class="item-name">resource</p>
                                                            <p class="item-type">(n)</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="dimmer"></div>
                                </div>
                                <div class="layer" wordid="">
                                    <div class="flash-card-viewport">
                                        <div class="flash-card-box" id="learning-theory-box-14" index="14">
                                            <figure class="slide jbs-current-1">
                                                <div class="learning-theory-flash-card-content">
                                                    <div class="item-content">
                                                        <div class="item-content-cover">
                                                            <p class="item-title"> Bạn biết từ vựng này hay không?</p>
                                                            <a class="item-sound main-sound-play" sound_url="" style="background: url(img/icons/speaker.png) 0px -165px; background-size: 78px;
                                                            padding-left: 60px;
                                                            margin-left: 10px;"></a>
                                                            <p class="item-name">resource</p>
                                                            <p class="item-type">(n)</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="dimmer"></div>
                                </div>
                                <div class="layer" wordid="">
                                    <div class="flash-card-viewport">
                                        <div class="flash-card-box" id="learning-theory-box-15" index="15">
                                            <figure class="slide jbs-current-1">
                                                <div class="learning-theory-flash-card-content">
                                                    <div class="item-content">
                                                        <div class="item-content-cover">
                                                            <p class="item-title"> Bạn biết từ vựng này hay không?</p>
                                                            <a class="item-sound main-sound-play" sound_url="" style="background: url(img/icons/speaker.png) 0px -165px; background-size: 78px;
                                                            padding-left: 60px;
                                                            margin-left: 10px;" i></a>
                                                            <p class="item-name">resource</p>
                                                            <p class="item-type">(n)</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="dimmer"></div>
                                </div>
                                <div class="layer" wordid="">
                                    <div class="flash-card-viewport">
                                        <div class="flash-card-box" id="learning-theory-box-16" index="16">
                                            <figure class="slide jbs-current-1">
                                                <div class="learning-theory-flash-card-content">
                                                    <div class="item-content">
                                                        <div class="item-content-cover">
                                                            <p class="item-title"> Bạn biết từ vựng này hay không?</p>
                                                            <a class="item-sound main-sound-play" sound_url="" style="background: url(img/icons/speaker.png) 0px -165px; background-size: 78px;
                                                            padding-left: 60px;
                                                            margin-left: 10px;" i></a>
                                                            <p class="item-name">resource</p>
                                                            <p class="item-type">(n)</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="dimmer"></div>
                                </div>
                                <div class="layer" wordid="">
                                    <div class="flash-card-viewport">
                                        <div class="flash-card-box" id="learning-theory-box-17" index="17">
                                            <figure class="slide jbs-current-1">
                                                <div class="learning-theory-flash-card-content">
                                                    <div class="item-content">
                                                        <div class="item-content-cover">
                                                            <p class="item-title"> Bạn biết từ vựng này hay không?</p>
                                                            <a class="item-sound main-sound-play" sound_url="" style="background: url(img/icons/speaker.png) 0px -165px; background-size: 78px;
                                                            padding-left: 60px;
                                                            margin-left: 10px;"></a>
                                                            <p class="item-name">resource</p>
                                                            <p class="item-type">(n)</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="dimmer"></div>
                                </div>
                                <div class="layer" wordid="">
                                    <div class="flash-card-viewport">
                                        <div class="flash-card-box" id="learning-theory-box-18" index="18">
                                            <figure class="slide jbs-current-1">
                                                <div class="learning-theory-flash-card-content">
                                                    <div class="item-content">
                                                        <div class="item-content-cover">
                                                            <p class="item-title"> Bạn biết từ vựng này hay không?</p>
                                                            <a class="item-sound main-sound-play" sound_url="" style="background: url(img/icons/speaker.png) 0px -165px; background-size: 78px;
                                                            padding-left: 60px;
                                                            margin-left: 10px;" i></a>
                                                            <p class="item-name">resource</p>
                                                            <p class="item-type">(n)</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="dimmer"></div>
                                </div>
                                <div class="layer" wordid="">
                                    <div class="flash-card-viewport">
                                        <div class="flash-card-box" id="learning-theory-box-19" index="19">
                                            <figure class="slide jbs-current-1">
                                                <div class="learning-theory-flash-card-content">
                                                    <div class="item-content">
                                                        <div class="item-content-cover">
                                                            <p class="item-title"> Bạn biết từ vựng này hay không?</p>
                                                            <a class="item-sound main-sound-play" sound_url="" style="background: url(img/icons/speaker.png) 0px -165px; background-size: 78px;
                                                            padding-left: 60px;
                                                            margin-left: 10px;"></a>
                                                            <p class="item-name">resource</p>
                                                            <p class="item-type">(n)</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="dimmer"></div>
                                </div>
                                <div class="layer" wordid="">
                                    <div class="flash-card-viewport">
                                        <div class="flash-card-box" id="learning-theory-box-20" index="20">
                                            <figure class="slide jbs-current-1">
                                                <div class="learning-theory-flash-card-content">
                                                    <div class="item-content">
                                                        <div class="item-content-cover">
                                                            <p class="item-title"> Bạn biết từ vựng này hay không?</p>
                                                            <a class="item-sound main-sound-play" sound_url="" style="background: url(img/icons/speaker.png) 0px -165px; background-size: 78px;
                                                            padding-left: 60px;
                                                            margin-left: 10px;"></a>
                                                            <p class="item-name">resource</p>
                                                            <p class="item-type">(n)</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="dimmer"></div>
                                </div>
                            </div>
                        </div>
                        <div class="learning-main-process">
                            <div class="learning-main-process-value"></div>
                        </div>
                        <div class="filter-word-bottom">
                            <div class="filter-word-button-cover">
                                <a href="#" class="filter-word-button button-no">Không biết</a>
                                <a href="#" id="biet" class="filter-word-button button-yes">Biết</a>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="hidden-userid" id="">
                <input type="hidden" name="hidden-topicid" id="">
                <input type="hidden" name="hidden-parent" id="">
                <audio id="learning-main-audio">
                    <source class="src" src="" type="audio/mpeg">
                </audio>
            </div>
        </div>
    </div>


    <!-- Bootstrap js -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $(document).scroll(function() {
                var pos = $(window).scrollTop();

                if (pos >= 100) {
                    $('.learning-tab').addClass('learning-tab-fixed');
                } else {
                    $('.learning-tab').removeClass('learning-tab-fixed');
                }
            });
            $(document).on("keydown", function(ev) {
                if (ev.keyCode === 27 || ev.keyCode === 122) return false;
            });

            var btnStart = $('.btn-start');
            var layoutOne = $('.layout-1');
            var layoutTwo = $('.layout-2');
            var btnBack = $('.button-back');
            var learningTab = $('.learning-tab');

            btnStart.on('click', function() {

                layoutOne.addClass("toggleCss");
                learningTab.addClass('show');
                layoutTwo.addClass('show');
            })

            btnBack.on('click', function() {
                layoutOne.removeClass('toggleCss');
                learningTab.removeClass('show');
            })
        });
    </script>
    <script type="text/javascript">
        $(window).scroll(function() {
            var pos = $(window).scrollTop();
            if (pos >= 2) {
                $('.learning-main-header').css('position', 'fixed');

                $('.learning-main-header').css('top', '0');

            } else {
                $('.learning-main-header').css('position', 'relative');
                $('.learning-main-header').css('top', '0');

            }
        });
    </script>

    <script type="text/javascript">
        window.kontext = function(container) {

            // Dispatched when the current layer changes
            var changed = new kontext.Signal();

            // All layers in this instance of kontext
            var layers = Array.prototype.slice.call(container.querySelectorAll('.layer'));

            // Flag if the browser is capable of handling our fancy transition
            var capable = 'WebkitPerspective' in document.body.style ||
                'MozPerspective' in document.body.style ||
                'msPerspective' in document.body.style ||
                'OPerspective' in document.body.style ||
                'perspective' in document.body.style;

            if (capable) {
                container.classList.add('capable');
            }

            // Create dimmer elements to fade out preceding slides
            layers.forEach(function(el, i) {
                if (!el.querySelector('.dimmer')) {
                    var dimmer = document.createElement('div');
                    dimmer.className = 'dimmer';
                    el.appendChild(dimmer);
                }
            });

            /**
             * Transitions to and shows the target layer.
             *
             * @param target index of layer or layer DOM element
             */
            function show(target, direction) {

                // Make sure our listing of available layers is up to date
                layers = Array.prototype.slice.call(container.querySelectorAll('.layer'));

                // Flag to CSS that we're ready to animate transitions
                container.classList.add('animate');

                // Flag which direction
                direction = direction || (target > getIndex() ? 'right' : 'left');

                // Accept multiple types of targets
                if (typeof target === 'string') target = parseInt(target);
                if (typeof target !== 'number') target = getIndex(target);

                // Enforce index bounds
                target = Math.max(Math.min(target, layers.length), 0);

                // Only navigate if were able to locate the target
                if (layers[target] && !layers[target].classList.contains('show')) {
                    layers.forEach(function(el, i) {
                        el.classList.remove('left', 'right');
                        el.classList.add(direction);
                        if (el.classList.contains('show')) {
                            el.classList.remove('show');
                            el.classList.add('hide1');
                            if (typeof $(el).find('.flash-card-box').boxSlider === "function") {
                                //Scroll to face 1 in learnning-theory
                                //$(el).find('.flash-card-box').boxSlider('showSlide', 0);
                            }

                        } else {
                            el.classList.remove('hide1');
                        }
                    });

                    layers[target].classList.add('show');

                    changed.dispatch(layers[target], target);

                }

            }

            /**
             * Shows the previous layer.
             */
            function prev() {

                var index = getIndex() - 1;
                show(index >= 0 ? index : layers.length + index, 'left');

            }

            /**
             * Shows the next layer.
             */
            function next() {

                show((getIndex() + 1) % layers.length, 'right');

            }

            /**
             * Retrieves the index of the current slide.
             *
             * @param of [optional] layer DOM element which index is
             * to be returned
             */
            function getIndex(of) {

                var index = 0;

                layers.forEach(function(layer, i) {
                    if ((of && of == layer) || (!of && layer.classList.contains('show'))) {
                        index = i;
                        return;
                    }
                });

                return index;

            }

            /**
             * Retrieves the total number of layers.
             */
            function getTotal() {

                return layers.length;

            }

            // API
            return {

                show: show,
                prev: prev,
                next: next,

                getIndex: getIndex,
                getTotal: getTotal,

                changed: changed

            };

        };

        /**
         * Minimal utility for dispatching signals (events).
         */
        kontext.Signal = function() {
            this.listeners = [];
        }

        kontext.Signal.prototype.add = function(callback) {
            this.listeners.push(callback);
        }

        kontext.Signal.prototype.remove = function(callback) {
            var i = this.listeners.indexOf(callback);

            if (i >= 0) this.listeners.splice(i, 1);
        }

        kontext.Signal.prototype.dispatch = function() {
            var args = Array.prototype.slice.call(arguments);
            this.listeners.forEach(function(f, i) {
                f.apply(null, args);
            });
        }
    </script>

    <script>
        var data = {};
        var index = 1;
        var topicId = $('#hidden-topicid').val();
        var userId = $('#hidden-userid').val();
        var parent = $('#hidden-parent').val();
        data.topicId = topicId;
        data.userId = userId;

        data.syn = [];
        $(document).ready(function() {
            var checkClick = 0;

            $(".btn-start").click(function() {
                $('.filter-word-item.item-current .item-sound').trigger('click');
                //$("html, body").animate({ scrollTop: 60 }, 600);
                $(this).parents(".layout-1 intro-topic-container").slideUp(200, function() {

                    checkClick = 1;
                });
            });

            var k = kontext(document.querySelector('#learning-theory-kontext'));

            updateProcess();



            // $(".filter-word-button-cover").click(function() {
            //     alert(1);
            //     if (checkClick == 1) {
            //         checkClick = 0;
            //         var wordCurrent = $(".layer.show.item-current");

            //         var id = $(wordCurrent).attr('wordId');
            //         var isKnow = 0;
            //         if ($(this).hasClass('button-yes')) {
            //             isKnow = 1;
            //         }

            //         data.syn.push({
            //             s_id: id,
            //             isKnow: isKnow
            //         });

            //         if (k.getIndex() < k.getTotal() - 1) {

            //             k.next();

            //             $(".layer").removeClass('item-current');
            //             $(".layer.show").addClass('item-current');
            //             $(".layer.show").find('.item-sound').trigger('click');
            //             setTimeout(function() {
            //                 checkClick = 1;
            //             }, 1500);

            //             updateProcess();
            //         } else {
            //             $.ajax({
            //                 url: urlConfig('filter-data'),
            //                 data: {
            //                     data: data
            //                 },
            //                 type: "POST"
            //             }).success(function(result) {
            //                 if (result == 0) {
            //                     location.reload();
            //                 } else {
            //                     setTimeout(function() {
            //                         window.location.href = urlConfig('show-filter-chart/' + topicId + '-' + parent);
            //                     }, 1000);
            //                 }

            //             });
            //         }
            //     }

            // });
            $('.filter-word-button-cover').click(function(e) {


                index += 1;
                if (index < 21) {
                    $('.kontext.capable').addClass('animate')
                    $('.layer').addClass('right');
                    $('.layer.show.item-current').addClass('hide1');
                    $('.hide1').removeClass('show');
                    $('.hide1').removeClass('item-current');
                    $('.hide1').prev().removeClass('hide1');
                    $('.hide1').next().addClass('show');
                    $('.hide1').next().addClass('item-current');
                    var src = $('.item-current .item-sound').attr('sound_url');
                } else {
                    $(this).children().attr('href', 'show-filter.html');
                }
                // alert(src);
                // $('#learning-main-audio').css('src', src);
                //playSound($('this'), cache_fcComplete);

            });
            $('#biet').click(function(e) {


            });
            $('.item-sound').click(function(e) {
                e.preventDefault();
                $(this).addClass('sound-active');
            });

            $(document).on("click", ".main-sound-play", function(event) {
                playSound(this, cache_fcComplete);
            });


            var cache_fcComplete = function() {};
            //play sound
            function playSound(element, endSound = function() {}) {
                cache_fcComplete = endSound;


                $(".main-sound-play.sound-active").removeClass('sound-active');
                var elCur = $(element);
                var src = $(element).attr('sound_url');
                var sound = $("#learning-main-audio");

                $(sound).find('source').attr('src', src);

                $(sound)[0].load();
                $(sound)[0].onloadeddata = function() {
                    $(sound)[0].play();
                    if (!$(elCur).hasClass('sound-active')) {
                        $(elCur).addClass('sound-active');
                    }

                    $(sound)[0].onended = function() {
                        $(elCur).removeClass('sound-active');
                        cache_fcComplete = function() {};
                        endSound();
                    };
                };
            }



            function updateProcess() {
                var total = k.getTotal();
                var index = k.getIndex();
                $(".learning-main-process-value").css('width', (index + 1) * 100 / total + "%");
                $(".filter-word-item.item-current").css('display', 'block');
            }
        });
    </script>

</body>

</html>